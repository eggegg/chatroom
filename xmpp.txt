<stream:stream
    xmlns='jabber:client'
    xmlns:stream='http://etherx.jabber.org/streams'
    id='s2c_234'
    from='140.112.18.203'
    version='1.0'>
<stream:features>
  <bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/>
  <session xmlns='urn:ietf:params:xml:ns:xmpp-session'/>
</stream:features>

<iq type='result' id='purplee05418f8'>
  <bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'>
    <jid>140.112.18.203</jid>
  </bind>
</iq>

<iq from='140.112.18.203'
    type='result'
    id='purplee05418f9'></iq>

<iq type='result'
    from='140.112.18.203'
    id='purplee05418fa'>
  <query xmlns='http://jabber.org/protocol/disco#items'>
</query>
</iq>


<iq type='result'
    from='140.112.18.203'
    id='purplee05418fb'>
  <query xmlns='http://jabber.org/protocol/disco#info'>
    <feature var='http://jabber.org/protocol/disco#info'/>
    <feature var='http://jabber.org/protocol/disco#items'/>
    <feature var='http://jabber.org/protocol/muc'/>
    <feature var='jabber:iq:register'/>
    <feature var='jabber:iq:search'/>
    <feature var='jabber:iq:time'/>
    <feature var='jabber:iq:version'/>
  </query>
</iq>


<iq from='140.112.18.203'
    type='result'
    id='purplee05418fc'>
    <vCard xmlns='vcard-temp'>
        <FN>peter</FN>
        <N>
            <GIVEN>peter</GIVEN>
            <FAMILY>shih</FAMILY>
            <MIDDLE/>
        </N>
        <NICKNAME>darkpi</NICKNAME>
<PHOTO><TYPE>image/png</TYPE><BINVAL>iVBORw0KGgoAAAANSUhEUgAAAGAAAAA0CAYAAAB1hz3ZAAAABHNCSVQICAgIfAhkiAAAFC9JREFUeNrtXEtvI9eV/qr4FklJfIkSHxIpiqRESupOw4bdsTfJIosEDgIEMGDASIDZZTPjxSQz/2AGmN+QbONVkCAIghhxjO643bIju6VukRRFkZLIpkiREpsU36+6s8jcmiqyqljKGJlNLiDAEPv4HJ5z77nn+865YgCAEPLPAP4J/1h/75XR/s9/rAC49w9//N0Xw6r5V4QQNJtNjEajqc84jkOj0QAhRFLu9vYW4/F46rPRaIRWqyWpj+M4vHr1SvL3t7e3kroAyOoihMjayHEcms2m5Ge9Xg/dbldS13A4lLSfEIJWqyXpK6nFqg3Vq1evcHp6OvX7UqmEly9fyspdXV0hl8tNGfns2TNJZ9HPj46OMBwORb8vFAqKuq6vr3F6ejrlzFqthkwmI70FGQZHR0fodDpTNiQSCdkAjMdjPHv2bErXcDjEl19+CYZhvr4AMAwDn8+HSqWCXq8nMuLs7AzhcFhSIcMwWF9fR7FY5J1JCEG9XgfLspifn5fUp9FosL6+jkwmw3/BWboAYG1tDeVyGf1+X7TDU6kUtre3ZW3c3t7G0dGRyJm1Wg0AYLPZJHUZjUYsLi6KNgQNWjgcBsuyX+8JYFkWW1tbIkPPzs7g8/mg1+tl5TQaDSKRCJLJJC+XSqUQi8UUd8nKygoajQba7TY
IIUin0wgEAtDpdIq64vE4byMhBC9fvoTdbofBYJCVs1gsMBqNuLq64oOWSCRkg0bX1tYW8vk8v7k6nQ5arRZWVla+3hNAl9Pp5PNzt9tFqVTC6urqTLmVlRW0Wi3c3t6iUCjAZrPBaDSqCvjx8TE6nQ6q1aoqXQ6HA4PBAI1GA6PRCNlsFpFIRNEhDMPwujiOQ7FYhMvlUtxYAKDVarG2tsanvcPDQ9y7d0+18wFAe6crm2EQj8fx7NkzWCwWbG1tqTpq9JgnEgmMx2N885vfVGXkwsICtFot9vb2cP/+fdW67t27h4ODA9hsNoTDYWi1s7+mTqfD6uoqcrkcyuUy3nzzTVU
2ejwe/PnPf8bc3ByMRiOsVuudyiCtsCqpVquyFYZwNZtN5PN5uFwuXF5eqlaWy+Xg9/v5o67KQK0WR0dH2NzcvJOuVquFXC6Hb3/726rlDAYDfvOb3+C1115DpVJRrcvlcuHDDz/Ee++9h1KpNLOitFqtsFqtYBjmfwOg0WjgcDhUKZybmwPHcbDb7dBoNKpker0ePB4PBoMBHA6H6mOaSqXwrW99C71eD36/X7VT5ubmYLfb4XA4VF+IvV4PPp8PGo0GTqdTta52u41AIMBfzLPWlM8IIf9BVK7Ly0vy4sULcn5+TpLJpCoZjuPI/v4+qdVqJJ1Ok0wmQziOmymXz+fJ0dERGQ6H5NNPPyX9fl+Vruvra/Lll1+Ss7OzO9n4+eefk3q9Tvb29sjt7a0quX6/T/74xz+SZrNJHj9+TMbjMbnDOrzTJTwej5HNZhGNRuH3+1GpVGTr5EkMwTAMFhcXEQqFUCwWMRgMFGWGwyF/gdKy9Pj4eGaKJIQgmUxia2sLq6urqFarqmy8ubmBRqPB/Pw84vE4nj9/rkrXixcvsL29DbPZDIfDgXK5rCqN37kKIoQgk8lgdXUVWq0WLMtK1s9SSDORSCAej4NhGLAsi2g0qihHCEEqlUIoFIJWqwXDMFheXka73Z4CTJMrn8/D6XTCaDSCZVlsbm4ikUjMtDGdTmNrawsMw8BsNsNsNqNSqSjKtVot9Pt9LC0tgWEYRCIRnJycgOO4rz8Ag8EA19fX8Pl8fP622+0Yj8eo1+uyjiwWi1haWuJLOoZh4Ha70e12ZamIXq+HV69ewev18rpoBSbEE1Kn5uLiQlR2ulwujMdjSWpDGDSXywWTycTrisViODk5Udwkh4eHIqyg1WoRDAZl5SilI/yMneRShChS6lgLLzSGYbCzs4MXL15IKhyNRjg/P0coFBJdugzDYHd3V/KY02O9s7MzdXlarVbodDrJCoWCtVAoJLrkqI1yp0DORr1eD5/Ph2w2KylXqVRgMpmmyk6/34+bmxt
JP3a7XSSTSfkTQDmaSYWNRgPD4RB2u12y2nA6nXj58qVIjqYsmkYml9Vqxdzc3FTOvLm5ASFEkgJgGAabm5s4PT2dOuadTgf1eh0ej0fSxoWFhSkeiQYtHA5LVnOBQACXl5dT9xXHcUgmk9jZ2Zmq5iiomww43Vg0zUkGYH5+HgaDAdVqdWr3SymjCiORCE5PT0XkGnXIysqKIqij6JN+saOjI+zu7sqWqQaDAcvLy8jn81Pk3fb2tmzJGYvFkMlkRAQfRedSQROmolQqxTuTEILT01Osrq5KImWGYWC329Hr9XB7eyvaWDqdburEsFI7LJ1O884slUpYXFxUpA5o7qPkGSW
l6MWrhD79fj9yuRwIISgUCnC73Yq6GIZBIBDAxcUF78ybmxtotVosLCwo1t7hcFhkI02rSjY6nU70ej3+vhoMBigUCggGg4o23r9/ny806CW/ubk5pYuVYvncbjfy+Tw4jsPJyYkiAylkIqvVKnq9Hmq1GnQ6nSzbOcmW5vN59Pt9ZDIZVbo0Gg2i0Sh/epLJ5MxgMwwDj8fD20h3pFLQhDQKveeorlngbm5uDjabDaVSCaVSCXa7nb/kRf9/ARD7d2GO++yzz+BwOGC1WuHz+VRVSvV6nS/D7t+/P5Nwo6tareLRo0d4+PAhvF6v6hJuf38fZrMZWq0W4XBYdV+DpqLXX399JuFGVzKZhMFgQKVSwcOHD1Xjpk8//RQajQYPHz6cKg4APBdxQaVSic/Her0eH330EX7wgx/g4uJCNVl3eHiI1dXVO/E9/X4f5+fniMfjqjtJ1Mbf/e53+OEPf6jaRgDIZDJwuVwzeRvhMplM+PnPf47333//TroqlQo4jhMVAIuLi1hYWBBzQVqtVsS11Go1xGIxOBwOWCwWVcpGoxHW19dhMBiwurqqmu/Z29vD+++/j9PTU2xtban+colEAg8ePIDValXN3QyHQ/j9fmg0mjvZSBlSvV6PtbU11RvL5/NhMBhgZWVF8rSxwt1LfxqNBjiOwxtvvIFEIjH1udQPAJycnCAej8Nut/MRnyVDKYDl5WVYLBaUy2VV+trtNhqNBt5++22k02kQQlTZmEqlEI1GYbPZ+EpqlhzHcTg+PsZ3vvMdlEol9Pt9VbqSySTC4TC2t7f5+n/y37ByNEA8Hsf8/Dz0er0qarbVaqHZbMLtdiMajSKbzcr2fCd3MS1xt7e3VfM9L168QDweh16vh8fjwdnZ2Uy5brcrsjGXy82kDQghyOVyfOcvGo3ym3IWQzoYDOB0OuFwOKbKUtkqqFgsYn5+HiaTSbJWlzPy+PiYR8parRahUEjRmYQQXFxcYGlpib+saVNkEso/ffoUf/jDH0TNd6PRyOf
RtbU1XF5eTjXxpRr9FGNoNBpsbGzwp0eJgikWizxSdjqdGA6Hks6cpCloZUbRuJQ/2MkcnsvlEI1GRRed1+tV3GE3NzfQ6/WistPv96NWq4ma+JO6pNqFwWAQpVJJhD4TiQTOz8/5yiKVSonqd9p3VnLm1dUV9Ho9D4QYhoHf70e1WpUl+OhJi8VifNlJaZREIiG5KQkhqFQqsFgsorvTbDbDZDJNZRNRAK6vr7GxsTFVLgUCAVkKmTpkEmTQ0yPFetITs7m5OVVPUzkK5X/961/jZz/7Gfb39/lTs7y8PFXiLi0todlsot1uS7KdJycnkjbu7u7KclmUG3O5XFMVkfC+kuL
NpHRtbm7i6OhIFDjRt3e73ZIdfbrDJo8QRa9yN7zdbgchhB/xEObiWq0Gj8cjyaW4XC50Oh0UCgV8+OGHfO7+5S9/iUKhgFAoJFkC7+7uSgY8n8/D4/FITkYsLi6CZdkptpTSIlJNdoZhEI1Gp+gXADyal/KHXq/H+vo6j/wlqQg5vsftdqPdboso5OFwiGKxiEAgICtHdxiNOiEEBwcHitMDFMp//vnn2NraQr/fx09+8hNwHIdoNCrbBqU8vrC3PRwOcX5+jvX1dVkbd3Z2cHh4KApcqVSCxWKB2WyWxSCBQEA0rDYajZDP5xGNRhVnl4SIWHU/QIp6TqVS2NjYUJw6MBgMcLvdKBQKIISgWq3CYDDMpADMZjM2NzdRLpfxwQcf4IMPPoDD4cDS0pKijdFolCfPhOlAiTqg9AsFWOPxGMfHxzNnl2hXsNfr8dVjMBhU1MWyrKjPcaeWJN0RV1dXaLVa6Ha7ig6hTgmHwzg7O8NwOFQ18ETlLBYLPvroI/z0pz/Fm2++iW63O5OD0el08Pl8uLi4QLvdRrfbhdvtnqkrEong7OwMo9EImUwG6+vrikNgkwHv9/uo1+si56qa+qCoV+1yu93Y39+H0WhEOBxW7DQJl81mw29/+1t4vV60223Jy3JyffLJJ3j33XdxdHSEtbU1DIdDVbYuLi7iiy++QDqdRiwWU22j0+nEkydP0Gw28fDhQ1W6tFotarUaPv744zv5w2w2w2Aw/JWMY1n2vwD86136wwJCCXeRu6vM3yr3f7HxrnJ/i8wvfvEL/OhHP3quBcD86U9/0o7HY1XNZJpXR6MR4vG4qqkzAMhmszCZTGg0Gnfiey4vL9Hr9TAajRCJRFQ7JJlMQqPRTF16s9B8NpuFwWDA5ubmnXRRYKiGk6J0Ok9H9/v9/wTwb2rGKc7Pz0EIgcViQbFYxP3791XB8oODA7z11lvY399HMBhUNQQ2HA6xt7eHt956C1988QW2trZmXt402BqNBi6XC8lkEq+//roqZz5
58gQPHjxAMplEIBBQZWOpVMKrV68QjUaxt7eHN954Y+ampEicZdnnEPQDZq5er0cePXpExuMx4TiOPH36lNTr9ZkDT0+ePCGNRoMQQkin0yGPHz8mo9FoptxXX31FyuUy4TiOtFotVYNP/X6ffPLJJ7yNBwcHpFwuz9RVKBTIwcEB/z0//vjjmbrG4zF59OgRPzB2cXFBUqmUqqGzOw9mEUJ4NMmyrKoxEUIIrq+vRRSA0Wjkm/iz0kGv1+Nnbubm5jA/P684+CTkpCimicViSKfTiul1NBrh9PQUsViMr/E9Hg/y+bwiTzQ5nu/1enF9fS05ESG0UfjKR3UAaNkphOUWiwU
mk0kSkk82y4XzPZFIBNlsVpY8I4Tg+fPnU3KxWEyRGGy1Wmi1WnzQhH0OIfqUSlnBYJBPHQzDYGNjA7lcTrZBNBgMUCqVEAgERLk9FovJUhsUB1FeS3UA5EYqhE6ZhORCGneStxH2dKUMLZfLU2QWrfHX19eRTqdlbZycqKBs6STBR1ev10O5XIbf7xfJabVaRCIR0UTEJGUvBbpsNhv/Jk3KxnQ6LULKrBRxNamwWq1KOoQ6JRAITLGlw+FQlrcB/vpo4/b2doqJ5DiO74xJlXXC5v9k689qtUpSByzLynJZ9KRJATyv14tGozFlY6fTQafTkRy5oX2NVColOqlC+l3IE01pzefzuLm5EbGdJycnsg6hO+zly5d87qOlmdLLFJZlp44rIQTZbBbLy8uyKJQOPglJN0odKHEwS0tL6Pf7aDaboiECAIrVziRPRNOq0uMUk8nEvx+jcqPRCMViERsbG/KDWRTp0jxLHeL1ehVLK+pMSiE3m010Oh1JtnMSsep0On4QbLL5IbeE856EEJyfn8Pn8ylSB5P0OB0aVuJ7GIbBwsICdDodP7FXq9VmziDRO+Ts7Iz3YyqVQiAQmCISJeeClpeXee6mUqmoal67XC6MRiM0m82pi3eWU+hxpa1QNXJ03nMwGODy8hLBYHCmnLCSKhQKWFxclGU7pXTRGaRZJJ3wvspkMuh2u7i9vZUcuWGFOYoel2AwiGKxiEQigWg0CpZl+c/lfgAgHo/j8ePH/FupWTKEEJ4tTSaT6Ha7PJKcJUeP+aNHj/jmhxobI5EIcrmcqPOnxsbl5WU8ffoULpcLRqNR1Xfzer24ubnB8+fPEY/Hp/zMk3E0RwnngnQ6HT777DN873vfU0Wc0Qs0k8nA4XDcaXZGr9fjV7/6Fd555507yQHA4eEhQqGQahtplUVH59Uug8GA3//+9/jxj398JxsJIfjqq6/4Z7c09U7NBdGGOBW6urrC7u4ubDabKvhPCMHZ2Rm++93volKp8LM3d5m5oVWOGlK
L4zj85S9/wTvvvIPhcCjbcJm0sdPpYGlpCRqNBisrK4rvhydB6Pe//330+31V45PUxmKxiAcPHmB+fl5y4puVayqbzWZ84xvfQDKZVEXS0ccRoVAIXq9Xdq5erqf89ttvo16vq97JV1dXMJlMiEQiKJfLss3/yUWHhiORyMyXM0LQdX19jddee41/PK6WSFxcXMS9e/dkXwSxcnA+Go3CbDZjfn5e1VGlQE2r1fJz9UqQnOo6PT3F2toadDod7t27J4siJ4NGp41ZlpV97DG56vU6OI6DzWaDw+HAaDRS/OMf1Ebhnx+Ix+NT7Uu5DUnvGZPJBIfDIUm/sFLo1e/3Q6fTiWb/lWY2m80m+v0+33mi78eUeCK6syqVCt9TtlqtMBqNioNg1MZgMMgDGnq0lZohky/ZhSheycZ2u41erweXy8U/NDSZTKI3FFK6stksVldX+TQciURwfn4+5Ud2MmrlclnEb+h0OoRCIcm/QkLzHH0pKMyLwj8ZoMSjC8ETHd1IpVKyU3X9fh+lUkk0xyp8iS+XLvP5vGgIjHalLBaL7JCu8BWk1Fs1ORvpxhLSG3IvPUUBqNVqkvU7fbwslw6cTufUyw9aP8s94BuNRjCbzfzOEuKQUCgkm9NbrZZkk31ubg4ej0eW4BuNRlOXJ+3pKsksLy9PfTe9Xg+/3y+bYm9vb7GzszM1X7WysgKTySQKAH0f8C/4x58s+/9Ymf8G24dNo8lMthgAAAAASUVORK5CYII=</BINVAL></PHOTO>
    </vCard>
</iq>

<iq type='error' id='purplee05418fd'>
  <error type='cancel'>
  </error>
</iq>

<iq type='result' id='purple2156204e'>
</iq>
<iq type='result' id='purple768c8615'>
</iq>

<message from='140.112.18.203/peter'><body>lol</body></message>
<message type='groupchat' from='123@140.112.18.203/google'><body>lol</body><html xmlns='http://jabber.org/protocol/xhtml-im'><body xmlns='http://www.w3.org/1999/xhtml'><a href='http://www.google.com'><p>lol</p></a><img src='http://www.gravatar.com/avatar/187995bfa27aa2bafd3ff843e6a33665?s=32&d=identicon&r=PG'/></body></html></message>
<message type='groupchat' from='test@140.112.18.203/peter'><body>lol</body></message>

<presence from='123@140.112.18.203/ee'></presence>
<presence from='peter'></presence>
<presence from='testroom123@140.112.18.203'></presence>
<presence from='test@140.112.18.203/peter50216' type='unavailable'><status>XD</status></presence>
<presence from='test@140.112.18.203/egg'></presence>
<presence from='peter'><status>XD</status></presence>
<presence from='testroom@140.112.18.203' type='unavailable'></presence>

<iq type='result'
    from='140.112.18.203'
    id='purple2156204b'>
  <query xmlns='http://jabber.org/protocol/disco#info'>
    <identity
        category='conference'
        type='text'
        name='Play-Specific Chatrooms'/>
    <identity
        category='directory'
        type='chatroom'
        name='Play-Specific Chatrooms'/>
    <feature var='http://jabber.org/protocol/disco#info'/>
    <feature var='http://jabber.org/protocol/disco#items'/>
    <feature var='http://jabber.org/protocol/muc'/>
    <feature var='jabber:iq:register'/>
    <feature var='jabber:iq:search'/>
    <feature var='jabber:iq:time'/>
    <feature var='jabber:iq:version'/>
  </query>
</iq>
