package chatroom.client;

import chatroom.client.dataclass.SearchableListModel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

/** RoomListPane class
 *
 * Shows room list.
 *
 * @author kelvin
 */
public class RoomListPane extends JPanel {
    /* room list for the list view */

    private JList roomList;
    /* scrollable pane */
    private JScrollPane listPane;
    /* parent object */
    // private ClientGUI parent;

    public void selectRoom(int ind) {
        roomList.setSelectedIndex(ind);
    }

    public void selectRoom(String name) {
        SearchableListModel model = ChatroomClient.getClient().getRoomListModel();
        if (name.length() > 0 && model.getSize() > 0) {
            int ind = model.longestMatch(name);
            if (ind > model.getSize()) {
                --ind;
            }
            roomList.setSelectedIndex(ind);
        }
    }

    public void refresh() {
        ChatroomClient.getClient().getRoomListModel().clear();
        ChatroomClient.getClient().getConnection().getRoomList();
    }

    public RoomListPane(ClientGUI entry) {
        // initialize
        // parent = entry;
        // room list
        roomList = new JList(ChatroomClient.getClient().getRoomListModel());
        roomList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = roomList.locationToIndex(e.getPoint());
                    if (index != -1) {
                        SearchableListModel lModel = (SearchableListModel) roomList.getModel();
                        RoomPane r = (RoomPane) lModel.getElementAt(index);
                        ChatroomClient client = ChatroomClient.getClient();
                        if (r.isOpened()) {
                            client.getEntry().enterRoom(r);
                        } else {
                            r.clearAll();
                            client.getConnection().enterRoom(r.getName(), client.getUserName());
                        }
                    }
                    //client.getEntry().enterRoom(r);
                    //r.updateUserList();
                }
            }
        });
        // scrollable pane
        listPane = new JScrollPane(roomList);
        listPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        listPane.setPreferredSize(new Dimension(130, 0));

        JPanel bottomPane = new JPanel();

        URL refreshIconURL = getClass().getResource("/images/refresh.png");
        ImageIcon refreshIcon = new ImageIcon(refreshIconURL);
        JButton refreshButton = new JButton(refreshIcon);
        refreshButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                refresh();
            }
        });
        refreshButton.setPreferredSize(new Dimension(20, 20));

        URL addIconURL = getClass().getResource("/images/add.png");
        ImageIcon addIcon = new ImageIcon(addIconURL);
        JButton addRoomButton = new JButton(addIcon);
        addRoomButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String s = (String) JOptionPane.showInputDialog(
                        RoomListPane.this, "Enter room name:", "Create Room",
                        JOptionPane.PLAIN_MESSAGE,
                        null, null, "new room");
                if (s != null) {
                    // Create room
                    ChatroomClient client = ChatroomClient.getClient();
                    RoomPane rr = client.getRoomListModel().get(s);
                    if (rr != null) {
                        // existing room: simply select
                        roomList.clearSelection();
                        roomList.setSelectedValue(rr, true);
                    }
                    RoomPane r = client.addRoom(s, true, true);
                    // Request entrance
                    client.getConnection().enterRoom(s, client.getUserName());
                    client.getEntry().enterRoom(r);
                    r.updateUserList();
                    roomList.clearSelection();
                    roomList.setSelectedValue(r, true);
                }
            }
        });
        addRoomButton.setPreferredSize(new Dimension(20, 20));


        final JTextField searchText = new JTextField();
        searchText.setPreferredSize(new Dimension(0, 20));
        /*        searchText.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
        selectRoom(searchText.getText());
        }
        });*/
        searchText.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                // do nothing
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // do nothing
            }

            @Override
            public void keyReleased(KeyEvent e) {
                selectRoom(searchText.getText());
            }
        });

        bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.X_AXIS));
        bottomPane.setPreferredSize(new Dimension(0, 20));
        bottomPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, 20));
        bottomPane.add(searchText);
        bottomPane.add(refreshButton);
        bottomPane.add(addRoomButton);
        bottomPane.setAlignmentX(CENTER_ALIGNMENT);

        // setting layout
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(new Color(200, 200, 200));
        // adding things
        JLabel title = new JLabel("List of Rooms:");
        title.setAlignmentX(CENTER_ALIGNMENT);
        title.setHorizontalAlignment(JLabel.LEFT);
        add(title);
        add(listPane);
        add(bottomPane);
    }
}
