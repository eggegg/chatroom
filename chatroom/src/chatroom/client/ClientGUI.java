package chatroom.client;

import chatroom.client.dataclass.ProfilePicResizer;
import chatroom.client.dataclass.User;
import chatroom.file.FileUploadConnection;
import chatroom.utils.ImageFilter;
import chatroom.utils.ImagePreview;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

/** ClientGUI class
 *
 * Handle every "global" prompt
 * i.e. Operations not restricted inside a room
 *
 * @author kelvin
 */
public final class ClientGUI extends JPanel {

    /** left panel */
    private JLabel profilePic;
    private JLabel statusBall;
    private RoomListPane roomListPane;
    private JPanel leftPane;
    static public final int profilePicSize = 128;
    static private final int leftPanelWidth = 130;
    /** middle panel */
    private TabbedPane roomTabbedPane;
    /** right panel */
    private UserPane rightPane;

    public void changeProfilePic(BufferedImage img) {
        //img = ProfilePicResizer.getPic(img, profilePicSize);
        ImageIcon pic = new ImageIcon(img);
        profilePic.setIcon(pic);
    }

    public RoomListPane getRoomListPane() {
        return roomListPane;
    }

    public void updateStatusBall() {
        String status = ChatroomClient.getClient().getUser().getStatus();
        URL statusIconURL = getClass().getResource("/images/" + status + ".png");
        ImageIcon statusIcon = new ImageIcon(statusIconURL);
        statusBall.setIcon(statusIcon);
    }

    public void changeProfilePic(File file) {
        BufferedImage img = null;
        //System.err.println("change pic!!");
        try {
            img = ImageIO.read(file);
            //ImageIcon pic;
            if (img.getWidth() >= profilePicSize || img.getHeight() >= profilePicSize) {
                int newWidth, newHeight;
                if (img.getWidth() > img.getHeight()) {
                    newWidth = profilePicSize;
                    newHeight = img.getHeight() * newWidth / img.getWidth();
                } else {
                    newHeight = profilePicSize;
                    newWidth = img.getWidth() * newHeight / img.getHeight();
                }
                BufferedImage resized = new BufferedImage(newWidth, newHeight, img.getType());
                Graphics2D g = resized.createGraphics();
                g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                g.drawImage(img, 0, 0, newWidth, newHeight, 0, 0, img.getWidth(), img.getHeight(), null);
                g.dispose();
                img = resized;
            }
            final BufferedImage img2 = img;
            FileUploadConnection.sendFile(ChatroomClient.getClient().getServerIP(), file, new FileUploadConnection.UploadCompleteAction() {

                @Override
                public void onComplete(String url) {
                    ChatroomClient.getClient().getConnection().updateProfilePic(img2, url);
                }
            });
            //pic = new ImageIcon(img);
            //profilePic = new JLabel(pic);
            //profilePic.setIcon(new ImageIcon(ImageIO.read(file)));
            //profilePic.setIcon(pic);
            changeProfilePic(img);
        } catch (IOException e) {
            System.err.println("failed to parse picture!!");
            // TODO: handle exception!! popup or something
        }
    }

    public ClientGUI() {
        setLayout(new BorderLayout());
        /* LINE_START */
        leftPane = new JPanel();
        // profile pic
        URL picURL = getClass().getResource("/images/faceless.png");
        profilePic = new JLabel();
        try {
            changeProfilePic(ImageIO.read(picURL));
        } catch (IOException ex) {
            Logger.getLogger(ClientGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        profilePic.setAlignmentX(Component.CENTER_ALIGNMENT);
        profilePic.setHorizontalAlignment(JLabel.CENTER);
        profilePic.setVerticalAlignment(JLabel.CENTER);
        profilePic.setMinimumSize(new Dimension(profilePicSize, profilePicSize));
        profilePic.setPreferredSize(new Dimension(profilePicSize, profilePicSize));
        profilePic.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                JFileChooser imageChooser = new JFileChooser();
                imageChooser.setFileFilter(new ImageFilter());
                imageChooser.setAccessory(new ImagePreview(imageChooser));
                int ret = imageChooser.showOpenDialog(ClientGUI.this);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File f = imageChooser.getSelectedFile();
                    changeProfilePic(f);
                }
            }
        });
        // userInfo
        JLabel nameLabel = new JLabel("[ " + ChatroomClient.getClient().getUserName() + " ]");
        //nameLabel.setPreferredSize(new Dimension(leftPanelWidth, 20));
        //nameLabel.setMaximumSize(new Dimension(leftPanelWidth, 20));
        //nameLabel.setMinimumSize(new Dimension(leftPanelWidth, 20));
        //nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        //nameLabel.setHorizontalAlignment(JLabel.CENTER);
        // status ball
        String status = ChatroomClient.getClient().getUser().getStatus();
        URL statusIconURL = getClass().getResource("/images/" + status + ".png");
        ImageIcon statusIcon = new ImageIcon(statusIconURL);
        statusBall = new JLabel(statusIcon);
        JPopupMenu statusMenu = new JPopupMenu("status");
        JMenuItem availableMenuItem = new JMenuItem(new StatusChangeAction(User.STATUS_AVAILABLE));
        availableMenuItem.setText("available");
        JMenuItem idleMenuItem = new JMenuItem(new StatusChangeAction(User.STATUS_IDLE));
        idleMenuItem.setText("idle");
        JMenuItem busyMenuItem = new JMenuItem(new StatusChangeAction(User.STATUS_BUSY));
        busyMenuItem.setText("busy");
        statusMenu.add(availableMenuItem);
        statusMenu.add(idleMenuItem);
        statusMenu.add(busyMenuItem);
        statusBall.addMouseListener(new DropdownListener(statusMenu));
        // roomListPane
        roomListPane = new RoomListPane(this);
        roomListPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        roomListPane.setMaximumSize(new Dimension(leftPanelWidth, Integer.MAX_VALUE));
        // roomListPane.setPreferredSize(new Dimension(leftPanelWidth, 0));
        // layout
        leftPane.setBackground(new Color(200, 200, 200));
        leftPane.setLayout(new BoxLayout(leftPane, BoxLayout.Y_AXIS));
        //leftPane.setPreferredSize(new Dimension(leftPanelWidth, 0));
        //leftPane.setMaximumSize(new Dimension(leftPanelWidth, 0));
        JPanel nameAndLabel = new JPanel();
        //leftPane.add(nameLabel);
        //nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        //nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameAndLabel.setLayout(new BoxLayout(nameAndLabel, BoxLayout.X_AXIS));
        nameAndLabel.add(Box.createRigidArea(new Dimension(5, 0)));
        nameAndLabel.add(nameLabel);
        nameAndLabel.add(Box.createHorizontalGlue());
        nameAndLabel.add(statusBall);
        nameAndLabel.add(Box.createRigidArea(new Dimension(5, 0)));
        leftPane.add(nameAndLabel);
        leftPane.add(profilePic);
        leftPane.add(roomListPane);
        leftPane.add(Box.createVerticalGlue());
        add(leftPane, BorderLayout.LINE_START);
        //add(roomListPane,BorderLayout.LINE_START);
        /* CENTER */
        roomTabbedPane = new TabbedPane(this);
        add(roomTabbedPane, BorderLayout.CENTER);
        /* LINE_END : n.a. */
    }

    /** Focus on a opened room */
    public void focusRoom(RoomPane room) {
        if (room == null) {
            return;
        }
        roomTabbedPane.setSelectedComponent(room);
        //rightPane.setUserList(room.getUsers());
    }

    private class StatusChangeAction extends AbstractAction {

        private String status;

        @Override
        public void actionPerformed(ActionEvent e) {
            ChatroomClient.getClient().changeStatus(status);
        }

        public StatusChangeAction(String status) {
            this.status = status;
        }
    }

    /** Open a tab for the room in the middle panel */
    public void enterRoom(RoomPane room) {
        if (room == null) {
            return;
        }
        if (!room.isOpened()) {
            //room.clearAll();
            roomTabbedPane.add(room.getName(), room);
            room.setOpened(true);
            /* TODO: close button? */
            /* TODO: initialize users in room */
        }
        focusRoom(room);
        ChatroomClient.getClient().updateStatusAtRoom(room.getName());
    }

    /** Leave (and close the tab) room */
    public void leaveRoom(RoomPane room) {
        if (room == null) {
            return;
        }
        // lobby cannot be closed
        if (room.isLobby()) {
            return;
        }
        if (room.isOpened()) {
            roomTabbedPane.remove(room);
            room.setOpened(false);
        }
    }

    private class DropdownListener extends MouseAdapter {

        private JPopupMenu menu;
        private Point cancelPos;

        @Override
        public void mousePressed(MouseEvent e) {
            Component c = e.getComponent();
            Rectangle r = c.getBounds();
            Rectangle bd = new Rectangle(c.getLocationOnScreen().x, c.getLocationOnScreen().y,
                    r.width, r.height);
            if (!bd.contains(cancelPos)) {
                menu.show(c, 0, r.height);
            }
            cancelPos = new Point(-1, -1);
        }

        public DropdownListener(JPopupMenu menu) {
            this.menu = menu;
            cancelPos = new Point(-1, -1);
            menu.addPopupMenuListener(new PopupMenuListener() {

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                    cancelPos = MouseInfo.getPointerInfo().getLocation();
                }

                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                    // nothing to do
                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    // nothing to do
                }
            });
        }
    }
}
