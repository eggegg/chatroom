package chatroom.client;

import chatroom.client.dataclass.ClientData;
import chatroom.client.dataclass.User;
import chatroom.connection.ClientConnection;
import chatroom.file.download.DownloadManager;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AbstractDocument.LeafElement;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/** RoomPane class
 *
 * Handle everything that happens inside a room
 * Responsible for keeping and maintaining chat log in the room
 *
 * @author kelvin
 */
public final class RoomPane extends JPanel implements ClientData {

    private String roomname;
    //private ArrayList<String> users;
    private boolean isPrivateRoom;
    private boolean isLobbyRoom;
    private boolean isOpenedRoom;
    //JTextPane chatLog;
    private JEditorPane chatLog;
    private JScrollPane logScrollPane;
    private HTMLTextEditor chatPane;
    //MyStyledDocument styleDoc;
    private HTMLDocument styleDoc;
    private UserPane userPane;
    private int chatLogHeight;
    private int tempUserNum;

    public void setTempUserNum(int tempNum) {
        this.tempUserNum = tempNum;
    }

    public int getUserNum() {
        if (isOpened()) {
            return userPane.getUserNum();
        } else {
            return tempUserNum;
        }
    }

    public boolean isOpened() {
        return isOpenedRoom;
    }

    public void setOpened(boolean b) {
        isOpenedRoom = b;
    }

    public boolean isPrivate() {
        return isPrivateRoom;
    }

    public boolean isLobby() {
        return isLobbyRoom;
    }

    /* NOTE: toString() is different from getName() by the appended userNum */
    @Override
    public String toString() {
        return roomname + " (" + getUserNum() + ")";
    }

    /*
     * Return the room name.
     */
    @Override
    public String getName() {
        return roomname;
    }

    public void addUser(User user) {
        String msg = user + " has entered the room.\n";
        userPane.addUser(user);
        systemMessage(msg);
    }

    public User getUser(String name) {
        return userPane.getUser(name);
    }

    public ArrayList<User> getUserList() {
        return userPane.getUserList();
    }

    public UserPane getUserPane() {
        return userPane;
    }

    public void removeUser(User user) {
        String msg = user + " has left the room.\n";
        userPane.removeUser(user);
        if (chatPane.getPrivateMessageTo().equals(user.getName())) {
            chatPane.setPrivateMessageTo(HTMLTextEditor.allStr);
        }
        systemMessage(msg);
    }

    public void removeUser(String uname) {
        removeUser(userPane.getUser(uname));
    }

    public void setUserList(ArrayList<User> list) {
        userPane.setUserList(list);
    }

    public void updateUserList() {
        userPane.updateUserList();
    }

    public void clearAll() {
        userPane.setUserList(new ArrayList<User>());
        chatLog.setText("");
        chatPane.clearAll();
    }


    /*public void incomeMessage(String msg, Style style) {
    try {
    styleDoc.insertString(styleDoc.getLength(), msg, style);
    } catch (BadLocationException ex) {
    Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    
    public void incomeMessage(String user, String msg, Style style) {
    String str = user + ": " + msg + "\n";
    incomeMessage(str, style);
    }*/
    public void appendUserSay(String user) {
        appendUserSay(user, false, null);
    }

    public void appendUserSay(String user, boolean isPrivate, String to) {
        String msg = "";
        if (isPrivate) {
            if (to == null) {
                msg = "<font color=\"#00AA00\" face=\"Monospaced\"><i>"
                        + user + "</i>: " + "</font>";
            } else {
                msg = "<font color=\"#0000AA\" face=\"Monospaced\"><i>To "
                        + to + "</i>: " + "</font>";
            }
        } else {
            msg = "<font color=\"#000000\" face=\"Monospaced\">"
                    + user + ": " + "</font>";
        }
        Element b = styleDoc.getElement(styleDoc.getDefaultRootElement(),
                StyleConstants.NameAttribute,
                HTML.Tag.BODY);
        Element e = null;
        for (int i = b.getElementCount() - 1; i >= 0; i--) {
            if ("first".equals(b.getElement(i).getAttributes().getAttribute(HTML.Attribute.CLASS))) {
                e = b.getElement(i);
                break;
            }
        }

        try {
            styleDoc.insertAfterStart(e, msg);
        } catch (BadLocationException ex) {
            Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void appendMessage(String msg) {
        Element e = styleDoc.getElement(styleDoc.getDefaultRootElement(),
                StyleConstants.NameAttribute,
                HTML.Tag.BODY);
        try {
            styleDoc.insertBeforeEnd(e, msg);
        } catch (BadLocationException ex) {
            Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void incomeMessage(String user, String msg) {
        appendMessage(msg);
        appendUserSay(user);
    }

    public void incomePrivateMessage(String user, String to, String msg) {
        appendMessage(msg);
        if (ChatroomClient.getClient().getUserName().equals(to)) {
            appendUserSay(user, true, null);
        } else {
            appendUserSay(user, true, to);
        }
    }

    public void systemMessage(String msg) {
        String htmlstr = "<p style=\"margin-top: 0\">"
                + "<font color=\"#ff0000\" face=\"Monospaced\">"
                + msg + "</font> </p>";
        appendMessage(htmlstr);
    }

    public void sendCurrentMessage() {
        if (chatPane.getLength() == 0) {
            return;
        }
        String msg = chatPane.getText();
        //System.err.println(msg);
        ClientConnection connection = ChatroomClient.getClient().getConnection();
        if (chatPane.isPrivate()) {
            connection.sendPrivateMessage(getName(), ChatroomClient.getClient().getUserName(), chatPane.getRecipient(), msg);
        } else {
            connection.sendGroupMessage(getName(), ChatroomClient.getClient().getUserName(), msg);
        }
        chatPane.clearAll();
    }

    public RoomPane(String roomname, boolean isPrivateRoom, boolean isLobbyRoom) {
        this.roomname = roomname;
        this.isPrivateRoom = isPrivateRoom;
        this.isLobbyRoom = isLobbyRoom;
        this.isOpenedRoom = false;

        //styleDoc = new MyStyledDocument();
        //chatLog = new JTextPane(styleDoc);

        chatLog = new JEditorPane();
        HTMLEditorKit editorKit = new HTMLEditorKit();
        styleDoc = (HTMLDocument) editorKit.createDefaultDocument();
        chatLog.setDocument(styleDoc);
        chatLog.setEditorKit(editorKit);
        chatLog.setEditable(false);
        chatLog.addHyperlinkListener(new ClickLinkListener());
        chatLog.addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                Rectangle rec = logScrollPane.getViewport().getViewRect();
                int newHeight = chatLog.getHeight();
                if (newHeight > chatLogHeight) {
                    chatLog.scrollRectToVisible(new Rectangle(rec.x, rec.y + rec.height + newHeight - chatLogHeight, 1, 1));
                }
                chatLogHeight = newHeight;
            }
        });
        styleDoc = (HTMLDocument) chatLog.getDocument();
        styleDoc.setTokenThreshold(0);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel topPane = new JPanel();
        topPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        topPane.setLayout(new BoxLayout(topPane, BoxLayout.X_AXIS));

        logScrollPane = new JScrollPane(chatLog);
        logScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        logScrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        userPane = new UserPane(this);
        userPane.setMaximumSize(new Dimension(120, Integer.MAX_VALUE));
        userPane.setPreferredSize(new Dimension(120, 0));
        topPane.add(logScrollPane);
        topPane.add(userPane);

        JPanel bottomPane = new JPanel();
        //bottomPane.setPreferredSize(new Dimension(Integer.MAX_VALUE, 100));
        bottomPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, 120));
        bottomPane.setPreferredSize(new Dimension(Integer.MAX_VALUE, 120));
        bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.X_AXIS));

        chatPane = new HTMLTextEditor(this);
        bottomPane.add(chatPane);

        chatPane.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                //throw new UnsupportedOperationException("Not supported yet.");
                char c = e.getKeyChar();
                if (c == '\n') {
                    RoomPane.this.sendCurrentMessage();
                }
                System.out.println("pressed: " + c);
            }

            @Override
            public void keyPressed(KeyEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void keyReleased(KeyEvent e) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        JButton sendButton = new JButton("send");
        sendButton.addActionListener(new MessageSentListener());
        sendButton.setPreferredSize(new Dimension(120, 120));
        sendButton.setMaximumSize(new Dimension(120, 120));

        //sendButton.addActionListener(new HTMLEditorKit.BoldAction());
        bottomPane.add(sendButton);

        add(topPane);
        add(bottomPane);

        chatLogHeight = chatLog.getHeight();
    }

    private class MessageSentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            sendCurrentMessage();
        }
    }

    private class ClickLinkListener implements HyperlinkListener {

        @Override
        public void hyperlinkUpdate(HyperlinkEvent e) {
            // TODO: if link starts with http://server_ip, do direct download (?)
            //       other, open browser.
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                String ip = "http://" + ChatroomClient.getClient().getServerIP();
                String url = e.getDescription();
                if (!url.startsWith("http://")) {
                    url = "http://" + url;
                }
                if (!url.startsWith(ip)) {
                    try {
                        Desktop.getDesktop().browse(new URI(url));
                    } catch (IOException ex) {
                        Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(RoomPane.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    // From our own server! try to do something clever. (like get the file name from the associated hidden title.)
                    LeafElement be = (LeafElement) e.getSourceElement();
                    Enumeration names = be.getAttributes().getAttributeNames();
                    String fileName = null;
                    while (names.hasMoreElements()) {
                        Object name = names.nextElement();
                        if (HTML.Tag.A.equals(name)) {
                            AttributeSet obj = (AttributeSet) be.getAttribute(name);
                            fileName = (String) obj.getAttribute(HTML.Attribute.TITLE);
                        }
                    }
                    System.err.println(fileName);
                    File file = null;
                    JFileChooser chooser = new JFileChooser();
                    chooser.setSelectedFile(new File(fileName));
                    int ret = chooser.showSaveDialog(RoomPane.this);
                    if (ret == JFileChooser.APPROVE_OPTION) {
                        file = chooser.getSelectedFile();
                        if (file.exists()) {
                            if (JOptionPane.showConfirmDialog(RoomPane.this, "The file already exist. Override?", "File exist", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                                return;
                            }
                        }
                        if (!DownloadManager.getDownloadManager().addDownload(url, file)) {
                            JOptionPane.showMessageDialog(RoomPane.this, "This download already existed in download list.");
                        }
                    }
                }
            }
        }
    }
}
