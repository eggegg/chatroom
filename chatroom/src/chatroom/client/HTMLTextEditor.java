/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.client;

/* HTMLDocumentEditor.java
 *  @author: Charles Bell
 *  @version: May 27, 2002
 */
import chatroom.file.FileUploadConnection;
import chatroom.utils.ImageFilter;
import chatroom.utils.ImagePreview;
import chatroom.utils.Utils;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

public final class HTMLTextEditor extends JPanel implements ActionListener {

    private HTMLDocument document;
    private HTMLEditorKit editorKit;
    private JTextPane textPane = new JTextPane();
    private boolean debug = false;
    //private File currentFile;
    /** Listener for the edits on the current document. */
//    protected UndoableEditListener undoHandler = new UndoHandler();
    /** UndoManager that we add edits to. */
//    protected UndoManager undo = new UndoManager();
//    private UndoAction undoAction = new UndoAction();
//    private RedoAction redoAction = new RedoAction();
    private Action cutAction = new DefaultEditorKit.CutAction();
    private Action copyAction = new DefaultEditorKit.CopyAction();
    private Action pasteAction = new DefaultEditorKit.PasteAction();
    private Action boldAction = new StyledEditorKit.BoldAction();
    private Action underlineAction = new StyledEditorKit.UnderlineAction();
    private Action italicAction = new StyledEditorKit.ItalicAction();
    private Action insertBreakAction = new DefaultEditorKit.InsertBreakAction();
    private HTMLEditorKit.InsertHTMLTextAction unorderedListAction = new HTMLEditorKit.InsertHTMLTextAction("Bullets", "<ul><li> </li></ul>", HTML.Tag.P, HTML.Tag.UL);
    private HTMLEditorKit.InsertHTMLTextAction bulletAction = new HTMLEditorKit.InsertHTMLTextAction("Bullets", "<li> </li>", HTML.Tag.UL, HTML.Tag.LI);
    private static ImageIcon[] smileyIcons;
    private static String[] smileyFileNames = null;
    private RoomPane parentRoom;
    public static final String allStr = "<all users>";
    private final JLabel whoLabel = new JLabel(allStr);

    {
        try {
            smileyFileNames = Utils.getResourceListing(HTMLTextEditor.class, "images/smiley");
        } catch (URISyntaxException ex) {
            Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
        smileyIcons = new ImageIcon[smileyFileNames.length];
        for (int i = 0; i < smileyFileNames.length; i++) {
            smileyIcons[i] = new ImageIcon(HTMLTextEditor.class.getResource("/images/smiley/" + smileyFileNames[i]));
        }
    }

    public HTMLTextEditor(RoomPane parentRoom) {
        this.parentRoom = parentRoom;
        //super("HTMLTextEditor");
        editorKit = new HTMLEditorKit();
        document = (HTMLDocument) editorKit.createDefaultDocument();
        init();
    }

    private void insertPic(String str) {
        String html = String.format("<img src='%s'/>", str);
        try {
            editorKit.insertHTML(document, textPane.getCaretPosition(), html, 0, 0, HTML.Tag.IMG);
        } catch (BadLocationException ex) {
            Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPrivateMessageTo() {
        return whoLabel.getText();
    }

    public void setPrivateMessageTo(String str) {
        whoLabel.setText(str);
    }

    public void init() {
        JPanel menuBar = new JPanel();
        menuBar.setLayout(new BoxLayout(menuBar, BoxLayout.X_AXIS));
        //JMenuBar menuBar = new JMenuBar();
        //getContentPane().add(menuBar, BorderLayout.NORTH);
        add(menuBar, BorderLayout.NORTH);
        /*JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenu helpMenu = new JMenu("Help");
         */
        JPopupMenu styleMenu = new JPopupMenu("Style");

        JButton menuStyleButton = new JButton("style");
        menuStyleButton.addMouseListener(new DropdownListener(styleMenu));
        menuBar.add(menuStyleButton);

        /*
        JPopupMenu alignMenu = new JPopupMenu("Align");
        
        JButton menuAlignButton = new JButton("align");
        menuAlignButton.addMouseListener(new DropdownListener(alignMenu));
        menuBar.add(menuAlignButton);*/

        JPopupMenu colorMenu = new JPopupMenu("Color");
        //JPopupMenu colorBackMenu = new JPopupMenu("Background Color");
        JPopupMenu fontMenu = new JPopupMenu("Font");

        JButton menuFontButton = new JButton("font");
        menuFontButton.addMouseListener(new DropdownListener(fontMenu));
        menuBar.add(menuFontButton);

        JButton menuColorButton = new JButton("color");
        menuColorButton.addMouseListener(new DropdownListener(colorMenu));
        menuBar.add(menuColorButton);

        /*JButton menuBackColorButton = new JButton("background color");
        menuBackColorButton.addMouseListener(new DropdownListener(colorBackMenu));
        menuBar.add(menuBackColorButton);*/

        JPopupMenu smileyMenu = new JPopupMenu("Smiley");
        smileyMenu.setLayout(new GridLayout(0, 5, 2, 2));
        for (int i = 0; i < smileyIcons.length; i++) {
            ImageIcon icon = smileyIcons[i];
            final String fileName = smileyFileNames[i];
            JButton jb = new JButton();
            jb.setIcon(icon);
            jb.setPreferredSize(new Dimension(20, 20));
            jb.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    insertPic("http://" + ChatroomClient.getClient().getServerIP() + "/smiley/" + fileName);
                }
            });
            smileyMenu.add(jb);
        }

        JButton menuSmileyButton = new JButton("smiley");
        menuSmileyButton.addMouseListener(new DropdownListener(smileyMenu));
        menuBar.add(menuSmileyButton);

        JButton insertPicButton = new JButton("picture...");
        insertPicButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser imageChooser = new JFileChooser();
                imageChooser.setFileFilter(new ImageFilter());
                imageChooser.setAccessory(new ImagePreview(imageChooser));
                int ret = imageChooser.showOpenDialog(HTMLTextEditor.this);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File f = imageChooser.getSelectedFile();
                    String str = null;
                    try {
                        str = FileUploadConnection.sendFile(ChatroomClient.getClient().getServerIP(), f);
                    } catch (IOException ex) {
                        Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    insertPic(str);
                }
            }
        });
        menuBar.add(insertPicButton);

        JButton insertLinkButton = new JButton("link...");
        insertLinkButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String ret = (String) JOptionPane.showInputDialog(HTMLTextEditor.this, "URL?", "Input URL",
                        JOptionPane.QUESTION_MESSAGE, null, null, "http://");
                if (ret == null) {
                    return;
                }
                String html = String.format("<a href='%s'>%s</a>", ret, ret);
                try {
                    editorKit.insertHTML(document, textPane.getCaretPosition(), html, 0, 0, HTML.Tag.A);
                } catch (BadLocationException ex) {
                    Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        menuBar.add(insertLinkButton);

        JButton insertFileButton = new JButton("file...");
        insertFileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int ret = fileChooser.showOpenDialog(HTMLTextEditor.this);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File f = fileChooser.getSelectedFile();
                    String str = null;
                    try {
                        str = FileUploadConnection.sendFile(ChatroomClient.getClient().getServerIP(), f);
                    } catch (IOException ex) {
                        Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String html = String.format("<a href='%s' title='%s'>%s</a>", str, f.getName(), f.getName());
                    try {
                        editorKit.insertHTML(document, textPane.getCaretPosition(), html, 0, 0, HTML.Tag.A);
                    } catch (BadLocationException ex) {
                        Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        menuBar.add(insertFileButton);
        JButton voipButton = new JButton("VoIP");
        voipButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                ChatroomClient.getClient().getConnection().getAudioPort(parentRoom.getName(), ChatroomClient.getClient().getUserName());
            }
        });
        menuBar.add(voipButton);

        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(new JLabel("to: "));
        JPanel whoPanel = new JPanel();
        whoPanel.setMaximumSize(new Dimension(140, 20));
        whoPanel.setPreferredSize(new Dimension(140, 20));
        whoPanel.setBackground(new Color(200, 200, 200));
        whoPanel.add(whoLabel);
        whoPanel.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                UserSelector userSelector = new UserSelector(parentRoom.getUserList(), whoLabel, ChatroomClient.getClient());
            }
        });
        menuBar.add(whoPanel);

        JMenuItem redTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Red", Color.red));
        JMenuItem orangeTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Orange", Color.orange));
        JMenuItem yellowTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Yellow", Color.yellow));
        JMenuItem greenTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Green", Color.green));
        JMenuItem blueTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Blue", Color.blue));
        JMenuItem cyanTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Cyan", Color.cyan));
        JMenuItem magentaTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Magenta", Color.magenta));
        JMenuItem blackTextItem = new JMenuItem(new StyledEditorKit.ForegroundAction("Black", Color.black));
        JMenuItem otherColorTextItem = new JMenuItem(new StyledEditorKit.StyledTextAction("Other...") {

            @Override
            public void actionPerformed(ActionEvent e) {
                Color cl = JColorChooser.showDialog(HTMLTextEditor.this, "Choose color", Color.black);
                if (cl != null) {
                    new StyledEditorKit.ForegroundAction("MyColor", cl).actionPerformed(e);
                }
            }
        });

        colorMenu.add(redTextItem);
        colorMenu.add(orangeTextItem);
        colorMenu.add(yellowTextItem);
        colorMenu.add(greenTextItem);
        colorMenu.add(blueTextItem);
        colorMenu.add(cyanTextItem);
        colorMenu.add(magentaTextItem);
        colorMenu.add(blackTextItem);
        colorMenu.add(otherColorTextItem);

        JMenu fontTypeMenu = new JMenu("Font Type");
        fontMenu.add(fontTypeMenu);

        String[] fontTypes = {"Monospaced", "Dialog", "SansSerif", "Serif", "DialogInput"};
        for (int i = 0; i < fontTypes.length; i++) {
            if (debug) {
                System.out.println(fontTypes[i]);
            }
            JMenuItem nextTypeItem = new JMenuItem(fontTypes[i]);
            nextTypeItem.setAction(new StyledEditorKit.FontFamilyAction(fontTypes[i], fontTypes[i]));
            fontTypeMenu.add(nextTypeItem);
        }

        JMenu fontSizeMenu = new JMenu("Font Size");
        fontMenu.add(fontSizeMenu);

        int[] fontSizes = {6, 8, 10, 12, 14, 16, 20, 24, 32, 36, 48, 72};
        for (int i = 0; i < fontSizes.length; i++) {
            if (debug) {
                System.out.println(fontSizes[i]);
            }
            JMenuItem nextSizeItem = new JMenuItem(String.valueOf(fontSizes[i]));
            nextSizeItem.setAction(new StyledEditorKit.FontSizeAction(String.valueOf(fontSizes[i]), fontSizes[i]));
            fontSizeMenu.add(nextSizeItem);
        }


        JMenuItem boldMenuItem = new JMenuItem(boldAction);
        JMenuItem underlineMenuItem = new JMenuItem(underlineAction);
        JMenuItem italicMenuItem = new JMenuItem(italicAction);

        boldMenuItem.setText("Bold");
        underlineMenuItem.setText("Underline");
        italicMenuItem.setText("Italic");

        styleMenu.add(boldMenuItem);
        styleMenu.add(underlineMenuItem);
        styleMenu.add(italicMenuItem);

        JMenuItem subscriptMenuItem = new JMenuItem(new SubscriptAction());
        JMenuItem superscriptMenuItem = new JMenuItem(new SuperscriptAction());
        JMenuItem strikeThroughMenuItem = new JMenuItem(new StrikeThroughAction());

        subscriptMenuItem.setText("Subscript");
        superscriptMenuItem.setText("Superscript");
        strikeThroughMenuItem.setText("StrikeThrough");

        styleMenu.add(subscriptMenuItem);
        styleMenu.add(superscriptMenuItem);
        styleMenu.add(strikeThroughMenuItem);

        textPane.setContentType("text/html");
        JScrollPane scrollPane = new JScrollPane(textPane);
        scrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        scrollPane.setMinimumSize(new Dimension(500, 100));

        /* bind the enter key */
        Action enterAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                parentRoom.sendCurrentMessage();
            }
        };
        textPane.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "ENTER");
        textPane.getActionMap().put("ENTER", enterAction);

        /* bind the shift-enter key */
        Action shiftEnterAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                int pos = textPane.getCaretPosition();
                System.out.println(pos);
                Element e = document.getCharacterElement(pos);
                try {
                    document.insertString(pos, "\n", null);
                } catch (BadLocationException ex) {
                    Logger.getLogger(HTMLTextEditor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        textPane.getInputMap().put(KeyStroke.getKeyStroke("shift ENTER"), "SHIFT_ENTER");
        textPane.getActionMap().put("SHIFT_ENTER", shiftEnterAction);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(menuBar);
        //getContentPane().add(toolPanel, BorderLayout.CENTER);
        add(scrollPane);
        //pack();
        //setLocationRelativeTo(null);
        startNewDocument();
        //show();
    }

    public int getLength() {
        return document.getLength();
    }

    public String getText() {
        return textPane.getText();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String actionCommand = ae.getActionCommand();
        if (debug) {
            int modifier = ae.getModifiers();
            long when = ae.getWhen();
            String parameter = ae.paramString();
            System.out.println("actionCommand: " + actionCommand);
            System.out.println("modifier: " + modifier);
            System.out.println("when: " + when);
            System.out.println("parameter: " + parameter);
        }
        if (actionCommand.compareTo("New") == 0) {
            startNewDocument();
        } else if (actionCommand.compareTo("Select All") == 0) {
            selectAll();
        }
    }

    /*   protected void resetUndoManager() {
    undo.discardAllEdits();
    undoAction.update();
    redoAction.update();
    }*/
    public void startNewDocument() {
        /*        Document oldDoc = textPane.getDocument();
        if (oldDoc != null) {
        oldDoc.removeUndoableEditListener(undoHandler);
        }*/
        editorKit = new HTMLEditorKit();
        document = (HTMLDocument) editorKit.createDefaultDocument();
        textPane.setDocument(document);
        //document = (HTMLDocument) textPane.getDocument();

        //currentFile = null;
        //textPane.getDocument().addUndoableEditListener(undoHandler);
        //resetUndoManager();
    }

    public void clearAll() {
        textPane.setText("");
    }

    public void selectAll() {
        textPane.selectAll();
    }

    private class SubscriptAction extends StyledEditorKit.StyledTextAction {

        public SubscriptAction() {
            super(StyleConstants.Subscript.toString());
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            JEditorPane editor = getEditor(ae);
            if (editor != null) {
                StyledEditorKit kit = getStyledEditorKit(editor);
                MutableAttributeSet attr = kit.getInputAttributes();
                boolean subscript = (StyleConstants.isSubscript(attr)) ? false : true;
                SimpleAttributeSet sas = new SimpleAttributeSet();
                StyleConstants.setSubscript(sas, subscript);
                setCharacterAttributes(editor, sas, false);
            }
        }
    }

    private class SuperscriptAction extends StyledEditorKit.StyledTextAction {

        public SuperscriptAction() {
            super(StyleConstants.Superscript.toString());
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            JEditorPane editor = getEditor(ae);
            if (editor != null) {
                StyledEditorKit kit = getStyledEditorKit(editor);
                MutableAttributeSet attr = kit.getInputAttributes();
                boolean superscript = (StyleConstants.isSuperscript(attr)) ? false : true;
                SimpleAttributeSet sas = new SimpleAttributeSet();
                StyleConstants.setSuperscript(sas, superscript);
                setCharacterAttributes(editor, sas, false);
            }
        }
    }

    private class StrikeThroughAction extends StyledEditorKit.StyledTextAction {

        public StrikeThroughAction() {
            super(StyleConstants.StrikeThrough.toString());
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            JEditorPane editor = getEditor(ae);
            if (editor != null) {
                StyledEditorKit kit = getStyledEditorKit(editor);
                MutableAttributeSet attr = kit.getInputAttributes();
                boolean strikeThrough = (StyleConstants.isStrikeThrough(attr)) ? false : true;
                SimpleAttributeSet sas = new SimpleAttributeSet();
                StyleConstants.setStrikeThrough(sas, strikeThrough);
                setCharacterAttributes(editor, sas, false);
            }
        }
    }

    public boolean isPrivate() {
        return !whoLabel.getText().equals(allStr);
    }

    public String getRecipient() {
        return whoLabel.getText();
    }

    /*
    private class HTMLFileFilter extends javax.swing.filechooser.FileFilter {
    
    @Override
    public boolean accept(File f) {
    return ((f.isDirectory()) || (f.getName().toLowerCase().indexOf(".htm") > 0));
    }
    
    @Override
    public String getDescription() {
    return "html";
    }
    }
     */
    /* Copy and modified from StyledEditorKit.ForegroundAction*/
    /*
    public static class BackgroundAction extends StyledEditorKit.StyledTextAction {
    
    public BackgroundAction(String nm, Color fg, JTextPane textPane) {
    super(nm);
    this.fg = fg;
    this.textPane = textPane;
    }
    
    public void actionPerformed(ActionEvent e) {
    JEditorPane editor = getEditor(e);
    if (editor != null) {
    HTMLDocument htmlDocument = (HTMLDocument) textPane.getDocument();
    Color fg = this.fg;
    MutableAttributeSet attr = new SimpleAttributeSet();
    StyleConstants.setBackground(attr, fg);
    //int start = textPane.getSelectionStart();
    //int end = textPane.getSelectionEnd();
    //htmlDocument.setParagraphAttributes(start, end - start, attr, true);
    setCharacterAttributes(editor, attr, false);
    }
    }
    private JTextPane textPane;
    private Color fg;
    }*/
    //class UndoHandler implements UndoableEditListener {
    /**
     * Messaged when the Document has created an edit, the edit is
     * added to <code>undo</code>, an instance of UndoManager.
     */
    /*   @Override
    public void undoableEditHappened(UndoableEditEvent e) {
    undo.addEdit(e.getEdit());
    undoAction.update();
    redoAction.update();
    }
    }*/

    /*class UndoAction extends AbstractAction {
    
    public UndoAction() {
    super("Undo");
    setEnabled(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    try {
    undo.undo();
    } catch (CannotUndoException ex) {
    System.out.println("Unable to undo: " + ex);
    ex.printStackTrace();
    }
    update();
    redoAction.update();
    }
    
    protected void update() {
    if (undo.canUndo()) {
    setEnabled(true);
    putValue(Action.NAME, undo.getUndoPresentationName());
    } else {
    setEnabled(false);
    putValue(Action.NAME, "Undo");
    }
    }
    }
    
    class RedoAction extends AbstractAction {
    
    public RedoAction() {
    super("Redo");
    setEnabled(false);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    try {
    undo.redo();
    } catch (CannotRedoException ex) {
    System.err.println("Unable to redo: " + ex);
    ex.printStackTrace();
    }
    update();
    undoAction.update();
    }
    
    protected void update() {
    if (undo.canRedo()) {
    setEnabled(true);
    putValue(Action.NAME, undo.getRedoPresentationName());
    } else {
    setEnabled(false);
    putValue(Action.NAME, "Redo");
    }
    }
    }*/
    private class DropdownListener extends MouseAdapter {

        private JPopupMenu menu;
        private Point cancelPos;

        @Override
        public void mousePressed(MouseEvent e) {
            Component c = e.getComponent();
            Rectangle r = c.getBounds();
            Rectangle bd = new Rectangle(c.getLocationOnScreen().x, c.getLocationOnScreen().y,
                    r.width, r.height);
            if (!bd.contains(cancelPos)) {
                menu.show(c, 0, r.height);
            }
            cancelPos = new Point(-1, -1);
        }

        public DropdownListener(JPopupMenu menu) {
            this.menu = menu;
            cancelPos = new Point(-1, -1);
            menu.addPopupMenuListener(new PopupMenuListener() {

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                    cancelPos = MouseInfo.getPointerInfo().getLocation();
                }

                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                    // nothing to do
                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    // nothing to do
                }
            });
        }
    }
}
