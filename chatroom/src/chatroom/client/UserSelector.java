/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.client;

import chatroom.client.dataclass.ProfilePicResizer;
import chatroom.client.dataclass.User;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

/**
 *
 * @author kelvin
 */
public final class UserSelector extends JDialog {
    // should we use InputVerifier?

    private JLabel labelToSet;

    public UserSelector(ArrayList<User> _userList, JLabel labelToSet, JFrame parent) {
        super(parent, true);
        ArrayList<User> userList = new ArrayList<User>(_userList);
        final int perRow = 5;
        final int maxRow = 4;
        final int titleHeight = 40;
        int itemNum = userList.size();
        int rowCount = (itemNum + perRow - 1) / perRow;
        int height = Math.min(rowCount, maxRow) * UserRenderer.photolen;
        setSize(new Dimension(perRow * UserRenderer.labellen + 5, height + titleHeight));
        setPreferredSize(new Dimension(perRow * UserRenderer.labellen + 5, height + titleHeight));
        Dimension dim = getSize();
        Dimension cldim = ChatroomClient.getClient().getSize();
        Point clpos = ChatroomClient.getClient().getLocation();
        /*System.err.println(dim);
        System.err.println(cldim);
        System.err.println(clpos);*/
        int x = clpos.x + (cldim.width - dim.width) / 2;
        int y = clpos.y + (cldim.height - dim.height) / 2;
        setLocation(x, y);
        setTitle("Select message recipient:");
        this.labelToSet = labelToSet;
        //JPanel selectPanel = new JPanel();
        // add dummy "all user"
        URL allIconURL = getClass().getResource("/images/all.png");
        BufferedImage allImage = null;
        try {
            allImage = ImageIO.read(allIconURL);
        } catch (IOException ex) {
            Logger.getLogger(UserSelector.class.getName()).log(Level.SEVERE, null, ex);
        }
        userList.add(0, new User(HTMLTextEditor.allStr, allImage));
        // remove the user himself
        String selfname = ChatroomClient.getClient().getUserName();
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getName().equals(selfname)) {
                userList.remove(i);
                break;
            }
        }
        final JList selectList = new JList(userList.toArray());
        selectList.setCellRenderer(new UserRenderer());
        //selectList.setLayout(new GridLayout(0,6));
        selectList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        selectList.setVisibleRowCount(-1);
        selectList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = selectList.locationToIndex(e.getPoint());
                    User u = (User) selectList.getModel().getElementAt(index);
                    UserSelector.this.finialize(u.getName());
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(selectList);
        //selectPanel.setLayout(new GridLayout(0,6));
        add(scrollPane);

        /*requestFocusInWindow();
         * TODO: dont lose focus
        addFocusListener(new FocusListener() {
        
        @Override
        public void focusGained(FocusEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
        }
        
        @Override
        public void focusLost(FocusEvent e) {
        System.err.println("la");
        UserSelector.this.requestFocusInWindow();
        }
        });*/

        //set to appear at middle
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
    }

    private void finialize(String text) {
        labelToSet.setText(text);
        this.dispose();
    }

    class UserRenderer extends JLabel implements ListCellRenderer {

        //private JLabel nameLabel = null;
        //private JLabel picLabel = null;
        public static final int photolen = 75;
        public static final int labellen = 220;

        public UserRenderer() {
            setOpaque(true);
            setPreferredSize(new Dimension(labellen, photolen));
        }

        @Override
        public Component getListCellRendererComponent(
                JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {

            User user = (User) value;
            // Display the text for this item
            setText(user.getName());
            // Set the correct image
            //BufferedImage pic = user.getSmallPic();
            //ImageIcon icon = new ImageIcon(pic);
            //this.setIcon(icon);
            setIcon(ProfilePicResizer.getIcon(user.getSmallPic(), photolen));
            // Draw the correct colors and font
            if (isSelected) {
                // Set the color and font for a selected item
                setBackground(new Color(230, 230, 255));
                setForeground(Color.black);
                setFont(new Font("Roman", Font.BOLD, 15));
            } else {
                // Set the color and font for an unselected item
                setBackground(Color.white);
                setForeground(Color.black);
                setFont(new Font("Roman", Font.PLAIN, 15));
            }
            return this;
        }
    }
}
