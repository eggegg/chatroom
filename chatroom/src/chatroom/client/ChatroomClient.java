package chatroom.client;

import chatroom.audio.manager.AudioManager;
import chatroom.client.dataclass.Factory;
import chatroom.client.dataclass.SearchableListModel;
import chatroom.client.dataclass.User;
import chatroom.connection.ClientConnection;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/** ChatroomClient class
 *
 * Frame class to move between different GUIs
 * e.g. from login screen to main lobby
 *
 * @author kelvin
 */
public class ChatroomClient extends JFrame implements EventInterface {

    /** logger */
    private static final Logger logger = Logger.getLogger("Client");
    /** singleton object */
    private static ChatroomClient chatroomClient = new ChatroomClient();
    /** login GUI object */
    private Login login;
    /** entry GUI object */
    private ClientGUI entry;
    /** user info */
    private User user;
    final SearchableListModel<RoomPane> roomList = new SearchableListModel<RoomPane>(new Factory<RoomPane>() {
        private HashMap<String, RoomPane> roomMap = new HashMap<String,RoomPane>();
        @Override
        public RoomPane createInstance(String s) {
            RoomPane r = roomMap.get(s);
            if(r == null) {
                r = new RoomPane(s, false, false);
                roomMap.put(s, r);
            }
            return r;
        }
    });
    private ClientConnection clientConnection = null;
    private String serverIP = null;

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                logger.info("Client started");
                ChatroomClient.getClient().setVisible(true);
            }
        });
    }

    /** Getting singleton client
     *
     * @return chatroom client
     */
    public static ChatroomClient getClient() {
        return chatroomClient;
    }

    public User getUser() {
        return user;
    }

    public String getUserName() {
        return user.getName();
    }

    @Override
    public void setProfilePic(String roomid, String userid, BufferedImage pic, String fullPicURL) {

        //throw new UnsupportedOperationException("Not supported yet.");
        //System.err.printf("got pic for %s, %s: %s\n", roomid, userid, fullPicURL);
        //System.err.println(pic);
        RoomPane r = getRoomListModel().get(roomid);
        if (r == null) {
            System.err.println("null room in setProfilePic, implement this better.");
            return;
        }
        User u = r.getUser(userid);
        if (u == null) {
            System.err.println("null user in setProfilePic, is presence handling ok?");
            return;
        }
        if (pic == null) { // simply use default "faceless" picture
            System.err.println("pic-null");
            u.setFaceless();
        } else {
            System.err.println("pic-real");
            System.err.println(fullPicURL);
            u.setSmallPic(pic);
            u.setLargePic(fullPicURL);
        }
        /* for testing purpose only. */
        /* System.err.printf("got pic for %s, %s\n", roomid, userid);
        try {
        ImageIO.write(pic, "PNG", new File("test.png"));
        //throw new UnsupportedOperationException("Not supported yet.");
        } catch (IOException ex) {
        Logger.getLogger(ChatroomClient.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    @Override
    public void notifyStatusChange(String roomid, String userid, String status, String show) {
        logger.info(String.format("room %s, user %s, changing status to '%s'", roomid, userid, show));
        RoomPane r = roomList.get(roomid);
        r.getUser(userid).setStatus(show);
        r.getUserPane().getUserListModel().notifyChange(userid);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void updateStatusAtRoom(String roomid) {
        getConnection().updateStatus(roomid, getUserName(), null, user.getPidginStatus());
    }

    @Override
    public void openAudio(int port) {
        try {
            AudioManager am = AudioManager.getManager(serverIP, port);
            if (am == null) {
                JOptionPane.showMessageDialog(this, "You're already in a VoIP.", "Audio error", JOptionPane.ERROR_MESSAGE);
            } else {
                am.setVisible(true);
            }
        } catch (IOException ex) {
            Logger.getLogger(ChatroomClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(ChatroomClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeStatus(String status) {
        user.setStatus(status);
        for (int i = 0; i < roomList.getSize(); i++) {
            RoomPane r = roomList.getElementAt(i);
            if (r.isOpened()) {
                getConnection().updateStatus(r.getName(), getUserName(), null, user.getPidginStatus());
            }
        }
        getEntry().updateStatusBall();
    }

    private class OnClose extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            if (clientConnection != null) {
                clientConnection.endStream();
            }
            System.exit(0);
        }
    }

    /** Start the new client */
    private ChatroomClient() {
        // initialize size
        setSize(new Dimension(1000, 600));
        // initialize Position
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        int w = getSize().width;
        int h = getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;
        setLocation(x, y);
        // exit on close
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new OnClose());

        /*File folder = new File("D:\\Users\\peter\\Documents\\git\\chatroom\\chatroom\\src\\images\\smiley");
        File[] allfile = folder.listFiles();
        for (File file : allfile) {
        if ("gif".equals(Utils.getExtension(file))) {
        try {
        FileUploadConnection.sendFile("140.112.18.203", file);
        } catch (IOException ex) {
        Logger.getLogger(ChatroomClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        }*/
        // create login page
        login = new Login(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    // establish connection
                    clientConnection = new ClientConnection(login.getServer(),
                            Integer.valueOf(login.getPort()));
                    clientConnection.start();
                } catch (IOException ex) {
                    Logger.getLogger(ChatroomClient.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
                serverIP = login.getServer();
                // store user object
                user = new User(login.getUsername());
                // remove pane
                ChatroomClient.this.getContentPane().removeAll();
                ((JComponent) ChatroomClient.this.getContentPane()).revalidate();
                ChatroomClient.this.repaint();
                startEntry();
            }
        });
        add(login);
    }

    /** All the stuffs after connecting to server */
    private void startEntry() {
        entry = new ClientGUI();
        add(entry);
        invalidate();
        validate();
        repaint();
        clientConnection.getRoomList();
    }

    public ClientGUI getEntry() {
        return entry;
    }

    public ClientConnection getConnection() {
        return clientConnection;
    }

    public SearchableListModel<RoomPane> getRoomListModel() {
        return roomList;
    }

    public String getServerIP() {
        return serverIP;
    }

    @Override
    public void userExitRoom(String roomid, String userid) {
        RoomPane room = roomList.get(roomid);
        if (room == null) {
            return;
        }
        room.removeUser(userid);
    }

    public RoomPane addRoom(String roomid, boolean isPrivate, boolean isLobby) {
        // check room existence
        // TODOen
        //RoomPane r = roomList.add(new RoomPane(roomid, false, false));
        RoomPane r = roomList.add(roomid);
        return r;
    }

    @Override
    public void userEnterRoom(String roomid, String userid) {
        RoomPane room = roomList.get(roomid);
        if (room != null && room.getUser(userid) != null) {
            // This is possible, and should be ignored.
            return;
        }
        if (room == null) {
            // TODO: private room?
            room = roomList.add(roomid);
            //room = roomList.get(roomid);
        }
        room.addUser(new User(userid));
        getConnection().getParticipantInfo(roomid, userid);
        if(getUserName().equals(userid)) {
            getEntry().enterRoom(room);
        }
    }

    @Override
    public void receiveRoomParticipants(String roomid, ArrayList<User> users) {
        roomList.get(roomid).setUserList(users);
        for (int i = 0; i < users.size(); i++) {
            getConnection().getParticipantInfo(roomid, users.get(i).getName());
        }
    }

    @Override
    public void feedMessage(String roomid, String sender, String msg) {
        //System.err.println(roomid);
        //System.err.println(sender);
        //System.err.println(msg);
        roomList.get(roomid).incomeMessage(sender, msg);
    }

    @Override
    public void feedPrivateMessage(String roomid, String sender, String to, String msg) {
        roomList.get(roomid).incomePrivateMessage(sender, to, msg);
    }

    @Override
    public void receiveRoomList(ArrayList<String> rooms, ArrayList<String> roomDescriptions, ArrayList<Integer> roomParticipantNumber) {
        //System.err.println("roomlist:" + rooms);
        roomList.initListByName(rooms);
        for (int i = 0; i < roomParticipantNumber.size(); i++) {
            RoomPane room = roomList.get(rooms.get(i));
            room.setTempUserNum(roomParticipantNumber.get(i));
        }
        roomList.notifyChangeAll();
        if(roomList.getSize()>0) {
            RoomListPane rlp = getEntry().getRoomListPane();
            rlp.selectRoom(0);
        }
    }

    @Override
    public void nameConflicted(String roomid, String userid) {
        logger.warning("Name conflicted.");
        JOptionPane.showMessageDialog(ChatroomClient.this,
                "Conflicting name ID in room.",
                "Request forbidden",
                JOptionPane.ERROR_MESSAGE);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
