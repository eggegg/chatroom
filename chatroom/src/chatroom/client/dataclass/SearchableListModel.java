/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.client.dataclass;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author kelvin
 */
public class SearchableListModel<T extends ClientData> extends AbstractListModel {

    private final ArrayList<T> arr = new ArrayList<T>();
    private final Factory<T> factory;

    public void notifyChangeAll() {
        this.fireContentsChanged(arr, 0, arr.size() - 1);
    }

    public void notifyChange(int ind) {
        this.fireContentsChanged(arr, ind, ind);
    }

    public void notifyChange(String name) {
        int ind = firstLEQ(name);
        if (ind < arr.size() && name.equals(arr.get(ind).getName())) {
            notifyChange(ind);
        }
    }

    private int matchlen(String a, String b) {
        int i;
        for (i = 0; i < a.length() && i < b.length(); i++) {
            if (a.charAt(i) != b.charAt(i)) {
                break;
            }
        }
        return i;
    }

    public int longestMatch(String name) {
        int ind = firstLEQ(name);
        while (ind > 0 && (ind == arr.size() || matchlen(arr.get(ind - 1).getName(), name)
                >= matchlen(arr.get(ind).getName(), name))) {
            ind--;
        }
        return ind;
    }

    public int firstLEQ(String name) {
        int l = -1;
        int r = arr.size();
        while (l < r - 1) {
            int m = (l + r) / 2;
            if (arr.get(m).getName().compareTo(name) >= 0) {
                r = m;
            } else {
                l = m;
            }
        }
        return r;
    }

    public int firstLEQ(T room) {
        return firstLEQ(room.getName());
    }

    public T get(String name) {
        int ind = firstLEQ(name);
        if (ind < arr.size() && name.equals(arr.get(ind).getName())) {
            return arr.get(ind);
        } else {
            return null;
        }
    }

    public final ArrayList<T> getList() {
        return arr;
    }

    public T add(T r) {
        /*System.err.println(getSize());
        System.err.println(r);
        System.err.println("I want to see this");*/
        int ind = firstLEQ(r);
        // check for repeat
        if (ind < arr.size() && arr.get(ind).getName().equals(r.getName())) {
            return arr.get(ind);
        }
        arr.add(ind, r);
        fireIntervalAdded(this, ind, ind);
        return r;
    }
    
    public T add(String name) {
        return add(factory.createInstance(name));
    }

    public void remove(String name) {
        int ind = firstLEQ(name);
        if (ind < 0 || ind >= arr.size()) {
            return;
        }
        if (name.equals(arr.get(ind).getName())) {
            arr.remove(ind);
            fireIntervalRemoved(this, ind, ind);
        }
    }

    public void remove(T r) {
        remove(r.getName());
    }

    public void initListByName(ArrayList<String> names) {
        int r = arr.size();
        arr.clear();
        if (r > 0) {
            fireIntervalRemoved(this, 0, r - 1);
        }
        System.err.println(names);
        System.err.println("initByName");
        for (int i = 0; i < names.size(); i++) {
            add(factory.createInstance(names.get(i)));
        }
    }
    
    public void initListByInstance(ArrayList<T> ele) {
        int r = arr.size();
        arr.clear();
        if (r > 0) {
            fireIntervalRemoved(this, 0, r - 1);
        }
        System.err.println("initByInstance");
        for (int i = 0; i < ele.size(); i++) {
            add(ele.get(i));
        }
    }

    @Override
    public T getElementAt(int index) {
        return arr.get(index);
    }

    @Override
    public int getSize() {
        return arr.size();
    }

    public SearchableListModel(Factory<T> factory) {
        this.factory = factory;
    }

    public void clear() {
        arr.clear();
    }
}
