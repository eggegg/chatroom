/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.client.dataclass;

/**
 *
 * @author kelvin
 */
public interface Factory<T> {
    T createInstance(String s);
}
