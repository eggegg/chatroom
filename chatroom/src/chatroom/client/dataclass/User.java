package chatroom.client.dataclass;

import chatroom.client.ChatroomClient;
import chatroom.client.ClientGUI;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/** User data class
 * Storing user related data
 *
 * @author kelvin
 */
public class User implements ClientData {

    public static String STATUS_AVAILABLE = "available";
    public static String STATUS_IDLE = "idle";
    public static String STATUS_BUSY = "busy";
    public static String PIDGIN_STATUS_AVAILABLE = "available";
    public static String PIDGIN_STATUS_IDLE = "away";
    public static String PIDGIN_STATUS_XIDLE = "xa";
    public static String PIDGIN_STATUS_BUSY = "dnd";
    /** name of user */
    private String username;
    private BufferedImage smallPic;
    private BufferedImage largePic;
    private String status;

    private static boolean isNMChatStatus(String s) {
        return s != null
                && (s.equals(STATUS_AVAILABLE)
                || s.equals(STATUS_IDLE)
                || s.equals(STATUS_BUSY));
    }

    private static String pidginToNMChat(String s) {
        if (s != null) {
            if (s.equals(PIDGIN_STATUS_AVAILABLE)) {
                return STATUS_AVAILABLE;
            }
            if (s.equals(PIDGIN_STATUS_IDLE) || s.equals(PIDGIN_STATUS_XIDLE)) {
                return STATUS_IDLE;
            }
            if (s.equals(PIDGIN_STATUS_BUSY)) {
                return STATUS_BUSY;
            }
        }
        return STATUS_BUSY;
    }

    private static String nmChatToPidgin(String s) {
        if (s.equals(STATUS_AVAILABLE)) {
            return PIDGIN_STATUS_AVAILABLE;
        }
        if (s.equals(STATUS_IDLE)) {
            return PIDGIN_STATUS_IDLE;
        }
        if (s.equals(STATUS_BUSY)) {
            return PIDGIN_STATUS_BUSY;
        }
        return null;
    }

    public String getStatus() {
        return status;
    }

    public String getPidginStatus() {
        return nmChatToPidgin(status);
    }

    public void setStatus(String status) {
        if (isNMChatStatus(status)) {
            this.status = status;
        } else {
            this.status = pidginToNMChat(status);
        }
    }

    public BufferedImage getSmallPic() {
        if (smallPic == null) {
            setFaceless();
        }
        return smallPic;
    }

    public BufferedImage getLargePic() {
        return largePic;
    }

    public void setSmallPic(BufferedImage pic) {
        smallPic = pic;
    }

    public void setLargePic(BufferedImage pic) {
        largePic = pic;
    }

    public void setLargePic(String url) {
        if (url == null) {
            largePic = null;
            return;
        }
        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, "bad url of profile pic", ex);
        }
        try {
            largePic = ImageIO.read(u);
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setFaceless() {
        URL picURL = getClass().getResource("/images/faceless.png");
        BufferedImage bi = null;
        try {
            bi = ImageIO.read(picURL);
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        bi = ProfilePicResizer.getPic(bi, ClientGUI.profilePicSize);
        if (ChatroomClient.getClient().getUserName().equals(username)) {
            ChatroomClient.getClient().getEntry().changeProfilePic(bi);
        }

        // careful for using same reference
        // modifying directly on smallPic/largePic could be dangerous
        smallPic = largePic = bi;
    }

    @Override
    public String toString() {
        return username;
    }

    /** Constructing a new user */
    @Override
    public String getName() {
        return username;
    }

    public User(String name) {
        username = name;
        status = User.STATUS_AVAILABLE;
    }

    public User(String name, BufferedImage img) {
        username = name;
        smallPic = largePic = img;
        status = User.STATUS_AVAILABLE;
    }
}
