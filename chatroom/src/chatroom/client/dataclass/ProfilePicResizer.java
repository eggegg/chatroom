/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.client.dataclass;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 *
 * @author kelvin
 */
public class ProfilePicResizer {
    public static BufferedImage getPic(final BufferedImage img, final int len) {
        //if (img.getWidth() >= len || img.getHeight() >= len) {
        int newWidth, newHeight;
        if (img.getWidth() > img.getHeight()) {
            newWidth = len;
            newHeight = img.getHeight() * newWidth / img.getWidth();
        } else {
            newHeight = len;
            newWidth = img.getWidth() * newHeight / img.getHeight();
        }
        BufferedImage resized = new BufferedImage(newWidth, newHeight, img.getType());
        Graphics2D g = resized.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        //System.err.printf("%d %d %d %d\n",newWidth,newHeight,img.getWidth(), img.getHeight());
        g.drawImage(img, 0, 0, newWidth, newHeight, 0, 0, img.getWidth(), img.getHeight(), null);
        g.dispose();
        return resized;
    }
    public static ImageIcon getIcon(final BufferedImage img, final int len) {
        return new ImageIcon(getPic(img,len));
    }
}
