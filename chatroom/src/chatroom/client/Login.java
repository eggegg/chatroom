package chatroom.client;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/** Login class
 *
 * Prompt for username & profile pics
 * then establish connection from server
 *
 * @author kelvin
 */
public class Login extends JPanel {

    /** logger */
    private static final Logger logger = Logger.getLogger("Login");
    /** action to perform when login is done */
    private ActionListener parentListener;
    /** username text field */
    private JTextField nameTextField;
    private JTextField serverTextField;
    private JTextField portTextField;

    public Login(ActionListener action) {
        /* Initialize items */
        parentListener = action;
        // getting the image icon
        URL iconURL = getClass().getResource("/images/nmalpha.png");
        ImageIcon icon = new ImageIcon(iconURL);
        // label for title
        JLabel title = new JLabel(icon);
        title.setAlignmentX(Component.CENTER_ALIGNMENT);
        // text field for username
        nameTextField = new JTextField("Enter Username Here.");
        nameTextField.setPreferredSize(new Dimension(200, 20));
        nameTextField.setHorizontalAlignment(JTextField.CENTER);
        nameTextField.setAlignmentX(Component.CENTER_ALIGNMENT);
        nameTextField.setMaximumSize(nameTextField.getPreferredSize());
        nameTextField.selectAll();

        // text field for server & port
        serverTextField = new JTextField("140.112.18.203");
        serverTextField.setPreferredSize(new Dimension(200, 20));
        serverTextField.setHorizontalAlignment(JTextField.CENTER);
        serverTextField.setAlignmentX(Component.CENTER_ALIGNMENT);
        serverTextField.setMaximumSize(nameTextField.getPreferredSize());
        portTextField = new JTextField("50216");
        portTextField.setPreferredSize(new Dimension(200, 20));
        portTextField.setHorizontalAlignment(JTextField.CENTER);
        portTextField.setAlignmentX(Component.CENTER_ALIGNMENT);
        portTextField.setMaximumSize(nameTextField.getPreferredSize());
        portTextField.setEditable(false);

        // login button
        final JButton loginButton = new JButton("Login");
        loginButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                logger.info("login button pressed");
                /* TODO: check for username integrity */
                if (!isValidUsername()) {
                    JOptionPane.showMessageDialog(Login.this,
                            "Username should only contains a-z, A-Z, 0-9.",
                            "Username error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                /* On valid: continue to lobby */
                Login.this.parentListener.actionPerformed(e);
            }
        });
        /* bind the enter key */
        Action enterAction = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                loginButton.doClick();
            }
        };
        nameTextField.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "ENTER");
        nameTextField.getActionMap().put("ENTER", enterAction);

        // layout
        setBackground(Color.white);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentY(Component.CENTER_ALIGNMENT);
        setMaximumSize(new Dimension(200, 200));
        // adding things
        Dimension gap = new Dimension(20, 20);
        add(Box.createVerticalGlue());
        add(title);
        add(new Box.Filler(gap, gap, gap));
        add(nameTextField);
        add(new Box.Filler(gap, gap, gap));
        add(serverTextField);
        add(new Box.Filler(gap, gap, gap));
        add(portTextField);
        add(new Box.Filler(gap, gap, gap));
        add(loginButton);
        add(Box.createVerticalGlue());
    }

    /** Get the username from the textbox */
    public String getUsername() {
        return nameTextField.getText();
    }

    public String getServer() {
        return serverTextField.getText();
    }

    public String getPort() {
        return portTextField.getText();
    }

    private boolean isValidUsername() {
        String name = getUsername();
        if (name.isEmpty()) {
            return false;
        }
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (!Character.isLetterOrDigit(c)) {
                return false;
            }
        }
        return true;
    }
}
