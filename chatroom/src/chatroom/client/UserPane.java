/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.client;

import chatroom.client.dataclass.Factory;
import chatroom.client.dataclass.SearchableListModel;
import chatroom.client.dataclass.User;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.ScrollPaneConstants;
import javax.swing.Timer;
import javax.swing.event.MouseInputAdapter;

/** RightPane
 *
 * Show user lists in a room.
 *
 * @author kelvin
 */
public class UserPane extends JPanel {

    final SearchableListModel<User> listModel = new SearchableListModel<User>(new Factory<User>() {

        @Override
        public User createInstance(String s) {
            return new User(s);
        }
    });
    private JList userList = new JList(listModel);
    private JScrollPane listPane;
    private RoomPane parentRoom;

    public void addUser(User user) {
        listModel.add(user);
    }

    public void removeUser(User user) {
        listModel.remove(user);
    }

    public int getUserNum() {
        return listModel.getSize();
    }

    public User getUser(String name) {
        return listModel.get(name);
    }

    public User getUser(int ind) {
        return listModel.getElementAt(ind);
    }

    public ArrayList<User> getUserList() {
        //System.err.println(users);
        return listModel.getList();
    }
    
    public SearchableListModel<User> getUserListModel() {
        //System.err.println(users);
        return listModel;
    }
    
    public void setUserList(ArrayList<User> users) {
        //System.err.println(users);
        listModel.initListByInstance(users);
    }

    public void updateUserList() {
        ChatroomClient.getClient().getConnection().getRoomParticipants(parentRoom.getName());
    }

    public UserPane(RoomPane parentRoom) {

        this.parentRoom = parentRoom;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(new JLabel("userlist"));
        //setMaximumSize(new Dimension(100, Integer.MAX_VALUE));
        //setPreferredSize(new Dimension(100, 0));

        //listModel = (DefaultListModel)userList.getModel();
        listPane = new JScrollPane(userList);
        //listPane.setPreferredSize(new Dimension(130,Integer.MAX_VALUE));
        listPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        //listPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        add(listPane);

        UserPaneListener userPaneListener = new UserPaneListener();
        userList.addMouseListener(userPaneListener);
        userList.addMouseMotionListener(userPaneListener);
        userList.setCellRenderer(new UserRenderer());

    }

    private JPanel getUserInfoPanel(int ind) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        User u = getUser(ind);
        String name = u.getName();
        BufferedImage pic = u.getSmallPic();
        ImageIcon picIcon = new ImageIcon(pic);
        JLabel nameLabel = new JLabel(name);
        nameLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        JLabel picLabel = new JLabel(picIcon);
        picLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        picLabel.setHorizontalAlignment(JLabel.CENTER);
        panel.add(nameLabel);
        panel.add(picLabel);
        return panel;
    }

    private JPanel getUserInfoPanelBig(int ind) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        User u = getUser(ind);
        String name = u.getName();
        BufferedImage pic = u.getLargePic();
        if (pic == null) {
            pic = u.getSmallPic();
        }
        ImageIcon picIcon = new ImageIcon(pic);
        JLabel nameLabel = new JLabel(name);
        nameLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        JLabel picLabel = new JLabel(picIcon);
        picLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        picLabel.setHorizontalAlignment(JLabel.CENTER);
        panel.add(nameLabel);
        panel.add(picLabel);
        return panel;
    }

    private class UserPaneListener extends MouseInputAdapter {

        int pind;
        Timer timer;
        Timer timerBig;
        Popup popup;

        public UserPaneListener() {
            pind = -1;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // cancel?
        }

        @Override
        public void mouseDragged(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        private void reset() {
            if (popup != null) {
                popup.hide();
            }
            if (timer != null) {
                timer.stop();
            }
            if (timerBig != null) {
                timerBig.stop();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            int ind = userList.locationToIndex(e.getPoint());
            if (ind >= 0 && !userList.getCellBounds(ind, ind).contains(e.getPoint())) {
                ind = pind = -1;
            }
            if (ind < 0) {
                reset();
                return;
            }
            if (ind != pind) {
                reset();
                pind = ind;
                TimedShowEvent popper = new TimedShowEvent(getUserInfoPanel(ind));
                timer = new Timer(1000, popper);
                timer.setRepeats(false);
                timer.start();
                TimedShowEvent popperBig = new TimedShowEvent(getUserInfoPanelBig(ind));
                timerBig = new Timer(4000, popperBig);
                timerBig.setRepeats(false);
                timerBig.start();
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (userList.getBounds().contains(e.getPoint())) {
                return;
            }
            pind = -1;
            reset();
        }

        private class TimedShowEvent implements ActionListener {

            private JPanel infoPanel;

            @Override
            public void actionPerformed(ActionEvent e) {
                Point p = MouseInfo.getPointerInfo().getLocation();
                PopupFactory factory = PopupFactory.getSharedInstance();
                if (popup != null) {
                    popup.hide();
                }
                popup = factory.getPopup(UserPane.this, infoPanel, p.x, p.y);
                popup.show();
            }

            public TimedShowEvent(JPanel infoPanel) {
                this.infoPanel = infoPanel;
            }
        }
    }
    
    class UserRenderer extends JLabel implements ListCellRenderer {

        //private JLabel nameLabel = null;
        //private JLabel picLabel = null;
        //public static final int photolen = 75;
        //public static final int labellen = 220;

        public UserRenderer() {
            setOpaque(true);
            //setPreferredSize(new Dimension(labellen, photolen));
        }

        @Override
        public Component getListCellRendererComponent(
                JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {

            User user = (User) value;
            // Display the text for this item
            String status = user.getStatus();
            URL statusIconURL = getClass().getResource("/images/" + status + ".png");
            ImageIcon statusIcon = new ImageIcon(statusIconURL);
            JLabel statusBall = new JLabel(statusIcon);
            // Set the correct image
            //BufferedImage pic = user.getSmallPic();
            //ImageIcon icon = new ImageIcon(pic);
            this.setIcon(statusIcon);
            this.setText(user.getName());
            //setIcon(ProfilePicResizer.getIcon(user.getSmallPic(), photolen));
            // Draw the correct colors and font
            if (isSelected) {
                // Set the color and font for a selected item
                setBackground(new Color(230, 230, 255));
                setForeground(Color.black);
                setFont(new Font("Roman", Font.BOLD, 13));
            } else {
                // Set the color and font for an unselected item
                setBackground(Color.white);
                setForeground(Color.black);
                setFont(new Font("Roman", Font.PLAIN, 13));
            }
            return this;
        }
    }
}
