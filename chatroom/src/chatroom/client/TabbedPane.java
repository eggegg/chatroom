package chatroom.client;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.plaf.TabbedPaneUI;

/**
 *
 * @author kelvin
 */
public class TabbedPane extends JTabbedPane {

    private boolean isDragging = false;
    private Image dragImage = null;
    private Point dragCoor = null;
    private int dragIndex;
    private ClientGUI entry;

    public TabbedPane(ClientGUI entry) {
        this.entry = entry;
        addChangeListener(new TabChangeListener());
        addMouseListener(new TabManipulationListener());
        addMouseMotionListener(new TabManipulationListener());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (isDragging && dragCoor != null && dragImage != null) {
            System.out.println("DRAW!!");
            System.out.println("" + dragCoor.x + " " + dragCoor.y);
            // if isDragging: draw dragged tab
            g.drawImage(dragImage, dragCoor.x, dragCoor.y, this);
            //g.drawImage(dragImage, dragCoor.x, 0, this);
        }
    }

    /*private class TabComponent {
    };*/
    private class TabChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            JTabbedPane pane = (JTabbedPane) e.getSource();
            RoomPane room = (RoomPane) pane.getSelectedComponent();
            entry.focusRoom(room);
        }
    };

    private class TabManipulationListener extends MouseInputAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            // Gets the tab index based on the mouse position
            if (!isDragging) {
                TabbedPane pane = TabbedPane.this;
                TabbedPaneUI paneUI = pane.getUI();
                dragIndex = paneUI.tabForCoordinate(pane, e.getX(), e.getY());
                if (dragIndex >= 0) {
                    Rectangle bounds = paneUI.getTabBounds(pane, dragIndex);
                    // Paint the tabbed pane to a buffer
                    Image totalImage = new BufferedImage(getWidth(), getHeight(),
                            BufferedImage.TYPE_INT_ARGB);
                    Graphics totalGraphics = totalImage.getGraphics();
                    totalGraphics.setClip(bounds);
                    // Don't be double buffered when painting to a static image.
                    setDoubleBuffered(false);
                    paintComponent(totalGraphics);
                    // Paint just the dragged tab to the buffer
                    dragImage = new BufferedImage(bounds.width, bounds.height,
                            BufferedImage.TYPE_INT_ARGB);
                    Graphics graphics = dragImage.getGraphics();
                    graphics.drawImage(totalImage, 0, 0, bounds.width, bounds.height, bounds.x, bounds.y, bounds.x + bounds.width, bounds.y + bounds.height, pane);
                    isDragging = true;
                    repaint();
                }
            } else {
                dragCoor = e.getPoint();
                repaint();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            isDragging = false;
            repaint();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            System.out.println("close plz!");
            if ((e.getModifiers() & MouseEvent.BUTTON2_MASK) != 0) {
                TabbedPane pane = TabbedPane.this;
                TabbedPaneUI paneUI = pane.getUI();
                // close tab
                int index = paneUI.tabForCoordinate(pane, e.getX(), e.getY());
                if (index < 0) {
                    return;
                }
                RoomPane room = (RoomPane) pane.getComponentAt(index);
                if (!room.isLobby()) {
                    ChatroomClient client = ChatroomClient.getClient();
                    client.getConnection().exitRoom(room.getName(), client.getUserName());
                    entry.leaveRoom(room);
                }
            }
        }
    }
}
