package chatroom.client;

import chatroom.client.dataclass.User;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Queue;

/** Event Parser
 *
 * Parse event directly received from socket
 * and invoke corresponding functions to process operation
 *
 * @author kelvin
 */
public interface EventInterface {

    //public void notifyNickChange();
    //public void notifyAvailabilityChange();
    //public void invitationReceived();
    /* correspond to sendInvitation */
    //public void invitationDeclined();
    //public void invitationAccepted();
    public void notifyStatusChange(String roomid, String userid, String status, String show);

    public void setProfilePic(String roomid, String userid, BufferedImage pic, String fullPicURL);

    public void userExitRoom(String roomid, String userid);

    public void userEnterRoom(String roomid, String userid);

    /* When trying to enter a room with duplicated name. */
    public void nameConflicted(String roomid, String userid);

    /* correspond to getRoomParticipants */
    public void receiveRoomParticipants(String roomid, ArrayList<User> users);
    /* correspond to getRoomList */

    public void receiveRoomList(ArrayList<String> rooms, ArrayList<String> roomDescriptions, ArrayList<Integer> roomParticipantNumber);

    public void feedMessage(String roomid, String sender, String msg);

    public void feedPrivateMessage(String roomid, String sender, String to, String msg);

    public void openAudio(int port);
}
