/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.file;

import chatroom.connection.Connection;
import chatroom.utils.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peter
 */
public class FileUploadConnection extends Connection {

    private String server = null;
    private UploadCompleteAction action = null;
    public String url = null;

    private FileUploadConnection(String server, int port, UploadCompleteAction action) throws IOException {
        super(server, port);
        this.server = server;
        this.action = action;
    }

    private FileUploadConnection(String server, int port) throws IOException {
        this(server, port, null);
    }

    @Override
    protected void onRead(byte[] data, int size) {
        // Nothing to do.
    }

    public static String sendFile(String serverIP, File file) throws IOException {
        return sendFile(serverIP, file, null);
    }

    public static String sendFile(String serverIP, File file, UploadCompleteAction action) throws IOException {
        FileUploadConnection fu = new FileUploadConnection(serverIP, 12345, action);
        fu.start();
        fu.url = fu.realSendFile(file);
        fu.end();
        return fu.url;
    }

    @Override
    public void afterClose() {
        if (action != null) {
            action.onComplete(url);
        }
    }

    private String realSendFile(File file) {
        String extension = Utils.getExtension(file);
        if (extension != null) {
            send(extension);
        }
        send(new byte[]{(byte) 255});
        MessageDigest md = null;
        try {
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(FileUploadConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            FileInputStream fi = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fi.read(buffer)) != -1) {
                send(buffer, 0, len);
                md.update(buffer, 0, len);
            }
            fi.close();
        } catch (IOException ex) {
            Logger.getLogger(FileUploadConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] mdbytes = md.digest();
        StringBuilder hexString = new StringBuilder();
        hexString.append("http://");
        hexString.append(server);
        hexString.append("/upload/");
        for (int i = 0; i < mdbytes.length; i++) {
            hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
        }
        if (extension != null) {
            hexString.append(".");
            hexString.append(extension);
        }
        return hexString.toString(); // TODO: return url
    }

    public interface UploadCompleteAction {

        public void onComplete(String url);
    }
}
