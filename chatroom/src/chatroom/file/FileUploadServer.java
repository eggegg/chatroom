/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.file;

import chatroom.connection.Connection;
import chatroom.connection.Listener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peter
 */
public class FileUploadServer extends Listener {

    @Override
    public void onConnection(SocketChannel channel) throws IOException {
        new FileConnection(channel).start();
    }

    public FileUploadServer(int port) throws IOException {
        super(port);
    }

    private class FileConnection extends Connection {

        private boolean extensionDone = false;
        private StringBuilder extension = new StringBuilder();
        private File tempFile = File.createTempFile("upload", ".tmp");
        private FileOutputStream fout = new FileOutputStream(tempFile);
        private MessageDigest md = null;

        public FileConnection(SocketChannel income) throws IOException {
            super(income);
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(FileUploadServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        protected void onRead(byte[] data, int size) {
            int lasti = 0;
            if (!extensionDone) {
                for (int i = 0; i < size; i++) {
                    if (data[i] == (byte) 255) {
                        extensionDone = true;
                        lasti = i + 1;
                        break;
                    }
                    extension.append((char) data[i]);
                }
            }
            if (extensionDone) {
                try {
                    fout.write(data, lasti, size - lasti);
                } catch (IOException ex) {
                    Logger.getLogger(FileUploadServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                md.update(data, lasti, size - lasti);
            }
        }

        @Override
        public synchronized void end() {
            super.end();
            try {
                fout.close();
            } catch (IOException ex) {
                Logger.getLogger(FileUploadServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            byte[] mdbytes = md.digest();
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < mdbytes.length; i++) {
                hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
            }
            String newFile = "/var/www/upload/" + hexString;
            if (!extension.toString().isEmpty()) {
                newFile = newFile + "." + extension.toString();
            }
            Logger.getLogger(FileUploadServer.class.getName()).info(String.format("Moving file %s to %s\n", tempFile.getAbsolutePath(), newFile));
            if (!tempFile.renameTo(new File(newFile))) {
                Logger.getLogger(FileUploadServer.class.getName()).warning("Can't move the file.");
            }
        }
    }
}
