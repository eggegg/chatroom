package chatroom.file.download;
// Modified from http://www.java-tips.org/java-se-tips/javax.swing/how-to-create-a-download-manager-in-java.html

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.net.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

// The Download Manager.
public class DownloadManager extends JFrame implements Observer {

    // Download table's data model.
    private DownloadsTableModel tableModel;
    // Table listing downloads.
    private JTable table;
    // These are the buttons for managing the selected download.
    private JButton pauseButton, resumeButton;
    private JButton cancelButton, clearButton;
    private JButton clearAllButton;
    // Currently selected download.
    private Download selectedDownload;
    // Flag for whether or not table selection is being cleared.
    private boolean clearing;
    private static DownloadManager downloadManager = new DownloadManager();

    public static DownloadManager getDownloadManager() {
        return downloadManager;
    }

    // Constructor for Download Manager.
    private DownloadManager() {
        // Set application title.
        setTitle("Download Manager");

        // Set window size.
        setSize(640, 480);

        // Handle window closing events.
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                actionExit();
            }
        });

        // Set up Downloads table.
        tableModel = new DownloadsTableModel();
        table = new JTable(tableModel);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                tableSelectionChanged();
            }
        });
        table.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();
                    if (row != -1) {
                        tableModel.executeElement(row);
                    }
                    // do some action
                }
            }
        });
        // Allow only one row at a time to be selected.
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // Set up ProgressBar as renderer for progress column.
        ProgressRenderer renderer = new ProgressRenderer(0, 100);

        renderer.setStringPainted(
                true); // show progress text
        table.setDefaultRenderer(JProgressBar.class, renderer);

        // Set table's row height large enough to fit JProgressBar.
        table.setRowHeight(
                (int) renderer.getPreferredSize().getHeight());

        // Set up downloads panel.
        JPanel downloadsPanel = new JPanel();

        downloadsPanel.setBorder(BorderFactory.createTitledBorder("Downloads"));
        downloadsPanel.setLayout(
                new BorderLayout());
        downloadsPanel.add(
                new JScrollPane(table), BorderLayout.CENTER);

        // Set up buttons panel.
        JPanel buttonsPanel = new JPanel();
        pauseButton = new JButton("Pause");

        pauseButton.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        actionPause();
                    }
                });
        pauseButton.setEnabled(
                false);
        buttonsPanel.add(pauseButton);
        resumeButton = new JButton("Resume");

        resumeButton.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        actionResume();
                    }
                });
        resumeButton.setEnabled(
                false);
        buttonsPanel.add(resumeButton);
        cancelButton = new JButton("Cancel");

        cancelButton.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        actionCancel();
                    }
                });
        cancelButton.setEnabled(
                false);
        buttonsPanel.add(cancelButton);
        clearButton = new JButton("Clear");

        clearButton.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        actionClear();
                    }
                });
        clearButton.setEnabled(
                false);
        buttonsPanel.add(clearButton);
        clearAllButton = new JButton("Clear All");

        clearAllButton.addActionListener(
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        actionClearAll();
                    }
                });
        buttonsPanel.add(clearAllButton);

        // Add panels to display.
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(downloadsPanel, BorderLayout.CENTER);
        getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
    }

    // Exit this program.
    private void actionExit() {
        this.setVisible(false);
    }

    /*
     * Add a new download.
     * @return false if the file already in download list.
     */
    public boolean addDownload(String url, File file) {
        this.setVisible(true);
        this.requestFocus();
        URL verifiedUrl = verifyUrl(url);
        boolean ret = true;
        if (verifiedUrl != null) {
            ret = tableModel.addDownload(new Download(verifiedUrl, file));
            updateButtons();
        } else {
            JOptionPane.showMessageDialog(this, "Invalid Download URL", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return ret;
    }

    // Verify download URL.
    private URL verifyUrl(String url) {
        // Only allow HTTP URLs.
        if (!url.toLowerCase().startsWith("http://")) {
            url = "http://" + url;
        }

        // Verify format of URL.
        URL verifiedUrl = null;
        try {
            verifiedUrl = new URL(url);
        } catch (Exception e) {
            return null;
        }

        // Make sure URL specifies a file.
        if (verifiedUrl.getFile().length() < 2) {
            return null;
        }

        return verifiedUrl;
    }

    // Called when table row selection changes.
    private void tableSelectionChanged() {
        /* Unregister from receiving notifications
        from the last selected download. */
        if (selectedDownload != null) {
            selectedDownload.deleteObserver(DownloadManager.this);
        }

        /* If not in the middle of clearing a download,
        set the selected download and register to
        receive notifications from it. */
        if (!clearing) {
            if (table.getSelectedRow() != -1) {
                selectedDownload = tableModel.getDownload(table.getSelectedRow());
                selectedDownload.addObserver(DownloadManager.this);
                updateButtons();
            } else {
                selectedDownload = null;
            }
        }
    }

    // Pause the selected download.
    private void actionPause() {
        selectedDownload.pause();
        updateButtons();
    }

    // Resume the selected download.
    private void actionResume() {
        selectedDownload.resume();
        updateButtons();
    }

    // Cancel the selected download.
    private void actionCancel() {
        selectedDownload.cancel();
        updateButtons();
    }

    // Clear the selected download.
    private void actionClear() {
        clearing = true;
        tableModel.clearDownload(table.getSelectedRow());
        clearing = false;
        selectedDownload = null;
        updateButtons();
    }

    // Clear all download.
    private void actionClearAll() {
        clearing = true;
        tableModel.clearAllDownload();
        clearing = false;
        selectedDownload = null;
        updateButtons();
    }

    /* Update each button's state based off of the
    currently selected download's status. */
    private void updateButtons() {
        if (selectedDownload != null) {
            int status = selectedDownload.getStatus();
            switch (status) {
                case Download.DOWNLOADING:
                    pauseButton.setEnabled(true);
                    resumeButton.setEnabled(false);
                    cancelButton.setEnabled(true);
                    clearButton.setEnabled(false);
                    break;
                case Download.PAUSED:
                    pauseButton.setEnabled(false);
                    resumeButton.setEnabled(true);
                    cancelButton.setEnabled(true);
                    clearButton.setEnabled(false);
                    break;
                case Download.ERROR:
                    pauseButton.setEnabled(false);
                    resumeButton.setEnabled(true);
                    cancelButton.setEnabled(false);
                    clearButton.setEnabled(true);
                    break;
                default: // COMPLETE or CANCELLED
                    pauseButton.setEnabled(false);
                    resumeButton.setEnabled(false);
                    cancelButton.setEnabled(false);
                    clearButton.setEnabled(true);
            }
        } else {
            // No download is selected in table.
            pauseButton.setEnabled(false);
            resumeButton.setEnabled(false);
            cancelButton.setEnabled(false);
            clearButton.setEnabled(false);
        }
        if (tableModel.getRowCount() > 0) {
            clearAllButton.setEnabled(true);
        } else {
            clearAllButton.setEnabled(false);
        }
    }

    /* Update is called when a Download notifies its
    observers of any changes. */
    @Override
    public void update(Observable o, Object arg) {
        // Update buttons if the selected download has changed.
        if (selectedDownload != null && selectedDownload.equals(o)) {
            updateButtons();
        }
    }

    // Run the Download Manager.
    // For testing purpose only.
    public static void main(String[] args) {
        downloadManager.setVisible(true);
        downloadManager.addDownload("http://builds.kolmafia.us/KoLmafia-9925.jar", new File("tmp.jar"));
    }
}
