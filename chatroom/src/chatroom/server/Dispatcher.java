package chatroom.server;

import chatroom.connection.Listener;
import chatroom.connection.ServerConnection;
import chatroom.connection.Connection;
import java.io.IOException;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/** Dispatcher for incoming server connection
 *
 * @author eggegg
 */
public class Dispatcher extends Listener {
    /** Logger */
    private static final Logger logger = Logger.getLogger("Dispatcher");

    /** Listen selector */
    private Selector selector;
    /** Shared server socket */
    private ServerSocketChannel channel;
    /** Thread pool */
    private Map<Integer, ServerConnection> connectionPool;

    /** Initialization */
    public Dispatcher(int port) throws IOException {
        super(port);
        connectionPool = new HashMap<Integer, ServerConnection>();
    }

    /** Getting the connection from the connection pool
     *
     * @param id the connection id
     * @return connection object
     */
    public Connection getConnectionById(int id) {
        return connectionPool.get(id);
    }

    /** Handles incoming TCP sockets */
    @Override
    public void onConnection(SocketChannel channel) throws IOException {
        ServerConnection cnt = new ServerConnection(channel);
        connectionPool.put(cnt.getConnectionId(), cnt);
        ChatroomServer.getServer().addUser(cnt);
        cnt.setDaemon(true);
        cnt.start();
    }
}
