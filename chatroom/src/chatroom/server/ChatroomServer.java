package chatroom.server;

import chatroom.connection.Connection;
import chatroom.connection.ServerConnection;
import chatroom.file.FileUploadServer;
import chatroom.server.dataclass.Room;
import chatroom.server.dataclass.User;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Chatroom Server
 *
 * @author eggegg
 */
public class ChatroomServer {
    /** File upload port */
    private static final int FILE_UPLOAD_PORT = 12345;
    /** XMPP port */
    private static final int XMPP_PORT = 50216;
    /** Logger */
    private static final Logger logger = Logger.getLogger("Server");
    /** Singleton */
    private static ChatroomServer chatroomServer = new ChatroomServer();

    /** Room list
     *
     * Map from room name to the room object
     */
    private Map<String, Room> roomList = new HashMap<String, Room>();
    /** User list
     *
     * Map from connection id to the user object
     */
    private Map<Integer, User> userList = new HashMap<Integer, User>();

    /** Main function for server
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Dispatcher dispatcher = new Dispatcher(XMPP_PORT);
            dispatcher.start();
            FileUploadServer uploadServer = new FileUploadServer(FILE_UPLOAD_PORT);
            uploadServer.start();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Cannot create server", ex);
        }
    }

    /** Getting singleton server
     *
     * @return chatroom server
     */
    public static ChatroomServer getServer() {
        return chatroomServer;
    }

    private ChatroomServer() {
        // dummy
    }

    public Room getRoom(String roomName) {
        if (roomList.get(roomName) == null) {
            // TODO: new room handle.
            roomList.put(roomName, new Room(roomName, ""));
        }
        return roomList.get(roomName);
    }

    public Set<String> getRoomList() {
        return roomList.keySet();
    }

    public Collection<User> getUserList() {
        return userList.values();
    }

    public User getUserByConnection(Connection cnt) {
        if (cnt instanceof ServerConnection) {
            return userList.get(cnt.getConnectionId());
        } else {
            return null;
        }
    }

    public void addUser(ServerConnection cnt) {
        userList.put(cnt.getConnectionId(), cnt.getUser());
    }

    public void removeUser(User user) {
        userList.remove(user.getConnection().getConnectionId());

        Set<Room> rooms = user.getRoomList();
        for (Room room : rooms) {
            String nick = user.getNicknameByRoom(room);
            room.removeUser(nick);
            room.broadcastPresence(nick, "unavailable");
        }
    }

    public void removeEmptyRoom(Room room) {
        logger.info(String.format("Room %s is empty, removing...", room.getName()));
        roomList.remove(room.getName());
    }
}
