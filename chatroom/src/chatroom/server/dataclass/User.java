package chatroom.server.dataclass;

import chatroom.connection.Connection;
import chatroom.connection.ServerConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/** User class
 *
 * @author kelvin
 */
public class User {

    private static final Logger logger = Logger.getLogger("User");
    /** Mapping nickname in different room */
    private Map<Room, String> nicknameMap = new HashMap<Room, String>();
    /** Connection */
    private ServerConnection connection;
    private String profilePic = null;
    private String profilePicType = null;
    private String profilePicURL = null;
    private String status = null;
    private String show = "available";

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    /** Initialize by connection
     *
     * @param cnt connection object
     */
    public User(ServerConnection cnt) {
        connection = cnt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /** Getting nickname by room name
     *
     * @param roomname roomname
     * @return nickname
     */
    public String getNicknameByRoom(Room room) {
        return nicknameMap.get(room);
    }

    /** Getter of connection
     *
     * @return connection object
     */
    public Connection getConnection() {
        return connection;
    }

    public void setProfilePic(String profilePic, String profilePicType, String profilePicURL) {
        this.profilePic = profilePic;
        this.profilePicType = profilePicType;
        this.profilePicURL = profilePicURL;
        // Stupid method for broadcasting profile pic. (And not standard way.)
        for (Room room : getRoomList()) {
            String vCard = getVCard(room);
            for (String username : room.getUserList()) {
                User user = room.getUser(username);
                user.sendIqVCard(room, getNicknameByRoom(room), vCard);
            }
        }
    }

    public void sendIqVCard(Room room, String sender, String vCard) {
        connection.sendIqVCard(room.getName(), sender, vCard);
    }
    private static String VCARD_PHOTO_START = "<PHOTO>";
    private static String VCARD_PHOTO_END = "</PHOTO>";
    private static String VCARD_PHOTO_BASE = "<TYPE>%s</TYPE><BINVAL>%s</BINVAL>";
    private static String VCARD_PHOTO_EXTVAL = "<EXTVAL>%s</EXTVAL>";
    private static String VCARD_NICKNAME = "<NICKNAME>%s</NICKNAME>";

    public String getVCard(Room roomid) {
        StringBuilder sb = new StringBuilder();
        if (roomid != null) {
            String nick = nicknameMap.get(roomid);
            if (nick != null) {
                sb.append(String.format(VCARD_NICKNAME, nick));
            }
        }
        if (profilePic != null) {
            sb.append(VCARD_PHOTO_START);
            sb.append(String.format(VCARD_PHOTO_BASE, profilePicType, profilePic));
            if (profilePicURL != null) {
                sb.append(String.format(VCARD_PHOTO_EXTVAL, profilePicURL));
            }
            sb.append(VCARD_PHOTO_END);
        }
        return sb.toString();
    }

    /** Getting the list of joined room
     *
     * @return room list
     */
    public Set<Room> getRoomList() {
        return nicknameMap.keySet();
    }

    /** Join room with nickname
     *
     * @param room the room
     * @param nickname nickname
     * @return false if the name already existed.
     */
    public int joinRoomWithNickname(Room room, String nickname) {
        if (room.getUser(nickname) == this) {
            return -1;
        }
        if (room.getUser(nickname) != null) {
            logger.warning(String.format("join room %s with duplicated nick %s", room.getName(), nickname));
            return 0;
        }
        logger.info(String.format("join room %s with nick %s", room.getName(), nickname));
        // FIXME: handle duplicated nickname
        nicknameMap.put(room, nickname);
        room.addUser(this);
        return 1;
    }

    public void exitRoom(Room room) {
        logger.info(String.format("exit room %s", room.getName()));
        // FIXME: handle duplicated nickname
        String nick = nicknameMap.get(room);
        nicknameMap.remove(room);
        room.removeUser(nick);
    }

    /** Send a message from a room */
    public void sendMessage(Room room, String sender, String type, String msg) {
        connection.sendMessage(room.getName(), getNicknameByRoom(room), sender, type, msg);
    }

    /** Send a message from a room */
    public void sendMessage(Room room, String sender, String to, String type, String msg) {
        connection.sendMessage(room.getName(), to, sender, type, msg);
    }

    /** Send presence from a room */
    public void sendPresence(Room room, String nickname, String sender, String type) {
        User user = room.getUser(sender);
        if (user != null) {
            connection.sendPresence(room.getName(), nickname, sender, type, user.getStatus(), user.getShow());
        } else {
            // possible when sending unavailable presence.
            connection.sendPresence(room.getName(), nickname, sender, type, null, null);
        }
    }

    /** Send presence conflict from a room */
    public void sendPresenceConflict(Room room, String to) {
        connection.sendPresenceConflict(room.getName(), to);
    }

    public void sendAudioPort(Room room, int port) {
        connection.sendIqPingPort(room.getName(), getNicknameByRoom(room), port);
    }
}
