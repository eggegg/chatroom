package chatroom.server.dataclass;

import chatroom.audio.AudioServer;
import chatroom.server.ChatroomServer;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** Room class
 *
 * @author kelvin
 */
public class Room {

    /** Unique room name */
    private String name;
    /** Room description */
    private String description;
    /** AudioServer */
    private AudioServer audioServer;
    /** User list */
    private Map<String, User> userList = new HashMap<String, User>();

    public Room(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /** Get the audio server port, create one if not exist */
    public int getAudioPort() throws IOException {
        if (audioServer == null || audioServer.isStopped()) {
            audioServer = AudioServer.getInstance();
            audioServer.startServer();
        }
        return audioServer.getPort();
    }

    /** Getter of name */
    public String getName() {
        return name;
    }

    /** Getter of description */
    public String getDescription() {
        return description;
    }

    /** Adding user to room
     *
     * @param user user object
     */
    public void addUser(User user) {
        userList.put(user.getNicknameByRoom(this), user);
    }

    /** Getting user object by nickname
     *
     * @param nickname nickname
     * @return user object
     */
    public User getUser(String nickname) {
        return userList.get(nickname);
    }

    /** Getting all the user in the room
     *
     * @return a set of nickname
     */
    public Set<String> getUserList() {
        return userList.keySet();
    }

    /** Remove a user from the room
     *
     * @param nickname user's nickname
     */
    public void removeUser(String nickname) {
        userList.remove(nickname);
        if (userList.isEmpty()) {
            ChatroomServer.getServer().removeEmptyRoom(this);
        }
    }

    /** Send message to a user */
    public void sendMessage(String msg, String nickname, String sender) {
        ((User) userList.get(nickname)).sendMessage(this, sender, null, msg);
    }

    /** Broadcast message
     *
     * @param msg message
     * @param sender the nickname of the sender
     */
    public void broadcast(String msg, String sender) {
        for (User user : userList.values()) {
            user.sendMessage(this, sender, "groupchat", msg);
        }
    }

    /** Broadcast presence
     *
     * @param msg message
     * @param sender the nickname of the sender
     */
    public void broadcastPresence(String sender, String type) {
        for (User user : userList.values()) {
            user.sendPresence(this, user.getNicknameByRoom(this), sender, type);
        }
    }
}
