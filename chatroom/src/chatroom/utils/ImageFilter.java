/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.utils;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author peter
 */
public class ImageFilter extends FileFilter {

    final String[] imageext = {"jpg", "gif", "png"};

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        String ext = Utils.getExtension(f);
        for (String ie : imageext) {
            if (ie.equals(ext)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "images (.jpg/.gif/.png)";
    }
}
