package chatroom.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Non-blocking connection class
 *
 * @author eggegg
 */
public abstract class Connection extends Thread {

    /** Logger */
    protected static final Logger logger = Logger.getLogger("Connection");
    //protected static FileHandler fh = null;

    /*static {
    try {
    fh = new FileHandler("out.log", true);
    logger.addHandler(fh);
    fh.setFormatter(new Formatter() {
    
    @Override
    public String format(LogRecord record) {
    return record.getMessage() + "\n";
    }
    });
    } catch (IOException ex) {
    Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
    } catch (SecurityException ex) {
    Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
    }
    }*/
    /** Socket timeout */
    private static final int timeout = 3000;
    /** Unique id */
    private static int globalId = 0;
    /** UTF-8 decode. */
    private static Charset utf8Encoder = Charset.forName("UTF-8");

    /** Generate unique id
     *
     * @return id in integer
     */
    private static synchronized int newConnectionId() {
        return globalId++;
    }
    /** Selector object */
    private Selector selector;
    /** Socket object */
    private SocketChannel channel;
    /** Pre-allocate buffer */
    private ByteBuffer buffer = ByteBuffer.allocate(8192);
    /** Pending changes */
    private final LinkedList<ByteBuffer> pending = new LinkedList<ByteBuffer>();
    /** Id of the connection */
    private int id;
    /** End flag for stopping the connection */
    private boolean endFlag = false;

    /** Initialize an object from a new socket connection for server
     *
     * @param socket socket from dispatcher
     * @throws IOException connection failure
     */
    public Connection(SocketChannel income) throws IOException {
        setupChannel(income);
    }

    /** Initialize a connection connecting to server
     *
     * @param server server name
     * @param port port number
     * @throws IOException connection failure
     */
    @SuppressWarnings("SleepWhileInLoop")
    public Connection(String server, int port) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        socketChannel.connect(new InetSocketAddress(server, port));
        setupChannel(socketChannel);
        while (!socketChannel.finishConnect()) {
            // Some hack to ensure that we're connected. Other solution?
            try {
                sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void setupChannel(SocketChannel income) throws IOException {
        id = newConnectionId();
        channel = income;

        selector = SelectorProvider.provider().openSelector();
        channel.socket().setSoTimeout(timeout);
        channel.register(selector, SelectionKey.OP_READ);
        SelectionKey key = channel.keyFor(selector);
        key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
    }

    /** Getting unique id represents the connection
     *
     * @return id number
     */
    public int getConnectionId() {
        return id;
    }

    /** Thread runner */
    @Override
    public void run() {
        try {
            boolean running = true;
            while (!selector.keys().isEmpty()) {
                selector.select();
                // terminate the thread
                if (endFlag && pending.isEmpty()) {
                    channel.close();
                    logger.info("end by endflag = true");
                    afterClose();
                    break;
                }

                for (SelectionKey key : selector.selectedKeys()) {
                    if (key.isValid() && key.isReadable()) {
                        read(key);
                    }
                    // key may become invalid after read(key);
                    if (key.isValid() && key.isWritable()) {
                        write(key);
                    }
                }
            }
            logger.info("Connection run end.");
        } catch (IOException ex) {
            logger.warning("Connection failed");
        }
    }

    /** Stop the thread */
    public void end() {
        endFlag = true;
        selector.wakeup();
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        // Clear out our read buffer so it's ready for new data
        buffer.clear();

        // Attempt to read off the channel
        int size;
        try {
            size = socketChannel.read(buffer);
        } catch (IOException e) {
            // The remote forcibly closed the connection, cancel
            // the selection key and close the channel.
            key.cancel();
            socketChannel.close();
            end();
            return;
        }

        if (size == -1) {
            // Remote entity shut the socket down cleanly. Do the
            // same from our end and cancel the channel.
            key.cancel();
            socketChannel.close();
            end();
            return;
        }

        // Handle the response
        onRead(buffer.array(), size);
    }

    /** Send data through the socket
     *
     * @param buf data array
     */
    public void send(byte[] buf, int offset, int length) {
        ByteBuffer bb = ByteBuffer.allocate(length);
        bb.put(buf, offset, length);
        bb.rewind();
        synchronized (pending) {
            pending.add(bb);
        }
        selector.wakeup();
    }

    public void send(byte[] buf) {
        send(buf, 0, buf.length);
    }

    /** Send string through the socket
     *
     * @param str message string
     */
    public void send(String str) {
        logger.info(str);
        send(str.getBytes(utf8Encoder));
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        if (!socketChannel.isOpen()) {
            return;
        }

        // Write until there's not more data ...
        synchronized (pending) {

            /*
            if (pending.isEmpty()) {
            return;
            }
            try {
            socketChannel.write(pending.toArray(new ByteBuffer[0]));
            } catch (IOException ex) {
            // The remote forcibly closed the connection, cancel
            // the selection key and close the channel.
            key.cancel();
            socketChannel.close();
            end();
            return;
            }
            while (!pending.isEmpty() && pending.getFirst().remaining() == 0) {
            pending.removeFirst();
            }*/
            while (!pending.isEmpty()) {
                ByteBuffer buf = pending.getFirst();
                try {
                    socketChannel.write(buf);
                } catch (IOException ex) {
                    // The remote forcibly closed the connection, cancel
                    // the selection key and close the channel.
                    key.cancel();
                    socketChannel.close();
                    end();
                    return;
                }
                if (buf.remaining() > 0) {
                    System.err.println("remaining > 0");
                    return;
                }
                pending.removeFirst();
            }
        }
    }

    public void afterClose() {
    }

    /** Reading callback when new data arrives
     *
     * @param data the incoming data
     * @param size the size (in byte) of data
     */
    protected abstract void onRead(byte[] data, int size);
}
