package chatroom.connection;

import chatroom.parser.ServerParser;
import chatroom.server.ChatroomServer;
import chatroom.server.dataclass.User;
import java.io.IOException;
import java.nio.channels.SocketChannel;

/** Server non-blocking connection
 *
 * @author eggegg
 */
public class ServerConnection extends Connection {

    private ServerParser parser;
    private User user;

    public ServerConnection(SocketChannel income) throws IOException {
        super(income);
        parser = new ServerParser(this);
        user = new User(this);
    }

    @Override
    @SuppressWarnings("CallToThreadRun")
    public void run() {
        parser.start();
        super.run();
    }

    @Override
    public void end() {
        ChatroomServer.getServer().removeUser(user);
        parser.end();
        super.end();
    }

    @Override
    protected void onRead(byte[] data, int size) {
        parser.addData(data, size);
    }

    /** Sending message */
    public void sendMessage(String roomName, String nickname, String sender, String type, String msg) {
        parser.onSendMessage(roomName, nickname, sender, type, msg);
    }

    /** Sending presence */
    public void sendPresence(String roomName, String nickname, String sender, String type, String status, String show) {
        parser.onSendPresence(roomName, nickname, sender, type, status, show);
    }

    /** Sending presence conflict */
    public void sendPresenceConflict(String roomName, String to) {
        parser.onSendPresenceConflict(roomName, to);
    }

    /** Sending VCard */
    public void sendIqVCard(String roomid, String sender, String vCard) {
        parser.onSendIqVCard("id", roomid, sender, vCard);
    }

    /** Sending Ping port */
    public void sendIqPingPort(String roomid, String sender, int port) {
        parser.onSendIqPingPort("id", roomid, sender, port);
    }

    /** Getter of user */
    public User getUser() {
        return user;
    }
}
