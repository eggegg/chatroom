package chatroom.connection;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.logging.Logger;

/** Listener for TCP sockets
 *
 * @author eggegg
 */
public abstract class Listener extends Thread {

    /** Logger */
    private static final Logger logger = Logger.getLogger("Listener");
    /** Listen selector */
    private Selector selector;
    /** Shared server socket */
    private ServerSocketChannel channel;
    /** Flag to stop the thread */
    private boolean endFlag;

    /** Initialize a TCP socket listener
     *
     * @throws IOException if the port cannot be bind
     */
    public Listener(int port) throws IOException {
        selector = SelectorProvider.provider().openSelector();
        channel = ServerSocketChannel.open();

        InetSocketAddress isa = new InetSocketAddress((InetAddress) null, port);
        channel.socket().bind(isa);
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_ACCEPT);
    }

    /** Thread for catching connection */
    @Override
    public void run() {
        while (true) {
            try {
                // wait the connection
                selector.select();

                // terminate the thread
                if (endFlag) {
                    return;
                }

                for (SelectionKey key : selector.selectedKeys()) {
                    if (key.isValid() && key.isAcceptable()) {
                        SocketChannel connect = ((ServerSocketChannel) key.channel()).accept();
                        connect.configureBlocking(false);
                        onConnection(connect);
                        logger.info("Connection came in");
                    }
                }
            } catch (IOException e) {
                logger.warning("Connection failed");
            }
        }
    }

    /** Stop the thread */
    public synchronized void end() {
        endFlag = true;
        selector.wakeup();
    }

    /** Callback when a connection came in */
    public abstract void onConnection(SocketChannel channel) throws IOException;
}
