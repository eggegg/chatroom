package chatroom.connection;

import chatroom.parser.ClientParser;
import chatroom.utils.Base64;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/** Client connection
 *
 * @author eggegg
 */
public class ClientConnection extends Connection {

    private String server;
    private int port;
    private ClientParser parser;

    public ClientConnection(String server, int port) throws IOException {
        super(server, port);
        this.server = server;
        this.port = port;
        this.parser = new ClientParser(this, server);
    }

    @Override
    @SuppressWarnings("CallToThreadRun")
    public void run() {
        parser.start();
        startStream();
        super.run();
    }

    @Override
    protected void onRead(byte[] data, int size) {
        parser.addData(data, size);
    }
    private static String STREAM_START = "<?xml version='1.0' ?><stream:stream to='%s' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>";

    public void startStream() {
        send(String.format(STREAM_START, server));
    }
    private static String STREAM_END = "</stream:stream>";

    public void endStream() {
        send(STREAM_END);
    }

    public void getRoomList() {
        parser.sendRoomListQuery("id");
    }

    public void getRoomParticipants(String roomid) {
        parser.sendRoomUserListQuery("id", roomid);
    }

    public void getParticipantInfo(String roomid, String userid) {// Mainly, profile picture.
        parser.sendUserVCardGet("id", roomid, userid);
    }

    /* Standard says: The input image size SHOULD be <= 96 x 96
     * <PHOTO> SHOULD NOT contains <EXTVAL>
     * So this actually doesn't follow the standard :D */
    public void updateProfilePic(final BufferedImage bi, final String profilePicURL) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    ByteArrayOutputStream bo = new ByteArrayOutputStream();
                    ImageIO.write(bi, "PNG", bo);
                    ByteArrayOutputStream base64bo = new ByteArrayOutputStream();
                    Base64.OutputStream base64os = new Base64.OutputStream(base64bo);
                    byte[] bytes = bo.toByteArray();
                    base64os.write(bytes, 0, bytes.length);
                    String profilePic = base64bo.toString();
                    String profilePicType = "image/png";
                    parser.sendUserVCardSet("id", profilePic, profilePicType, profilePicURL);
                } catch (IOException ex) {
                    Logger.getLogger(ClientConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }

    public void enterRoom(String roomid, String name) {
        parser.sendPresenceAvailble(roomid, name);
    }

    public void exitRoom(String roomid, String name) {
        parser.sendPresenceUnavailble(roomid, name);
    }

    public void sendGroupMessage(String roomid, String name, String msg) {
        parser.sendGroupMessage(roomid, name, msg);
    }

    public void sendPrivateMessage(String roomid, String name, String to, String msg) {
        parser.sendPrivateMessage(roomid, name, to, msg);
    }

    public void updateStatus(String roomid, String name, String status, String show) {
        parser.updateStatus(roomid, name, status, show);
    }

    public void getAudioPort(String roomid, String name) {
        parser.getAudioPort("id", roomid, name);
    }
}
