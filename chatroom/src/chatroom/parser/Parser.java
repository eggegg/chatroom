/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.parser;

import chatroom.connection.Connection;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/** Generic parser
 *
 * @author eggegg
 */
public abstract class Parser extends Thread {

    /** Logger */
    protected static final Logger logger = Logger.getLogger("Parser");
    protected static FileHandler fh = null;

    static {
        try {
            fh = new FileHandler("in.log", true);
            logger.addHandler(fh);
            fh.setFormatter(new Formatter() {

                @Override
                public String format(LogRecord record) {
                    return record.getMessage() + "\n";
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /** Serial connection id */
    protected static int conId = 0;
    /** Connction object */
    protected Connection connection = null;
    /** Server ip */
    protected String serverIP = null;
    /** Input pipe */
    protected PipedInputStream is = null;
    /** Output pipe */
    protected PipedOutputStream os = null;
    /** Xml parser */
    protected XmlPullParser xmlParser = null;

    /** Initialize a server parser
     *
     * @param con connection object
     */
    public Parser(Connection con) {
        // create i/o stream
        is = new PipedInputStream();
        try {
            os = new PipedOutputStream(is);
        } catch (IOException ex) {
            logger.warning("Output stream cannot be created");
        }

        connection = con;
        xmlParser = new MXParser();
        try {
            xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            //xmlParser.setFeature("XML ROUNDTRIP", true);
            xmlParser.setInput(is, "UTF-8");
        } catch (XmlPullParserException ex) {
            logger.warning("Xml parser initialize error");
        }
    }
    protected boolean endFlag = false;

    public void end() {
        try {
            endFlag = true;
            is.close();
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** Get the data from the connection
     *
     * @param data data in byte array
     * @param size size of array (in byte)
     */
    public void addData(byte[] data, int size) {
        logger.info(new String(data, 0, size));
        try {
            os.write(data, 0, size);
            os.flush();
        } catch (IOException ex) {
            logger.warning("Output stream writing error");
        }
    }

    /** Parse name from bind tag
     *
     * @return resource name
     * @throws Exception parse error
     */
    protected String onBind() throws Exception {
        String resource = null;
        while (true) {
            int eventType = xmlParser.next();
            String tagName = xmlParser.getName();

            if (eventType == XmlPullParser.START_TAG && "resource".equals(tagName)) {
                resource = xmlParser.nextText();
            }
            if (eventType == XmlPullParser.END_TAG && "bind".equals(tagName)) {
                break;
            }
        }
        return resource;
    }
    protected static String IQ_QUERY_INFO_NS = "http://jabber.org/protocol/disco#info";
    protected static String IQ_QUERY_ITEM_NS = "http://jabber.org/protocol/disco#items";

    /** Parse the iq tag
     *
     * @throws Exception parse error
     */
    protected void onIq() throws Exception {
        // iq id, from id, to id
        String id = xmlParser.getAttributeValue("", "id");
        String from = xmlParser.getAttributeValue("", "from");
        String to = xmlParser.getAttributeValue("", "to");
        String type = xmlParser.getAttributeValue("", "type");
        if (to == null) {
            to = serverIP;
        }

        while (true) {
            int eventType = xmlParser.next();
            String tagName = xmlParser.getName();
            String nsName = xmlParser.getNamespace();

            if (eventType == XmlPullParser.START_TAG) {
                if ("bind".equals(tagName)) {
                    onIqBind(id, from, to, onBind());
                } else if ("session".equals(tagName)) {
                    onIqSession(id, from, to);
                } else if ("query".equals(tagName)) {
                    if (IQ_QUERY_INFO_NS.equals(nsName)) {
                        onIqQueryInfo(id, from, to);
                    } else if (IQ_QUERY_ITEM_NS.equals(nsName)) {
                        onIqQueryItem(id, from, to);
                    } else {
                        onIqQueryError(id, from, to);
                    }
                } else if ("ping".equals(tagName)) {
                    String port = xmlParser.getAttributeValue("", "port");
                    if (port != null) {
                        onIqPingPort(id, from, to, port);
                    } else {
                        onIqPing(id, from, to);
                    }
                } else if ("vCard".equals(tagName)) {
                    if ("set".equals(type)) {
                        onIqCardSet(id, from, to);
                    } else if ("get".equals(type)) {
                        onIqCardGet(id, from, to);
                    } else if ("result".equals(type)) {
                        onIqCardResult(id, from, to);
                    }
                } else if ("time".equals(tagName)) {
                    onIqQueryError(id, from, to);
                }
            }
            if (eventType == XmlPullParser.END_TAG && "iq".equals(tagName)) {
                break;
            }
        }
    }

    protected void onPresence() throws Exception {
        String from = xmlParser.getAttributeValue("", "from");
        String to = xmlParser.getAttributeValue("", "to");
        String type = xmlParser.getAttributeValue("", "type");
        if (to == null) {
            to = serverIP;
        }
        boolean isError = false;
        String errorType = null;
        if ("error".equals(type)) {
            isError = true;
        }
        String status = null;
        String show = "available";

        while (true) {
            int eventType = xmlParser.next();
            String tagName = xmlParser.getName();
            String nsName = xmlParser.getNamespace();

            if (eventType == XmlPullParser.START_TAG) {
                if ("status".equals(tagName) && !"unavailable".equals(type)) {
                    status = xmlParser.nextText();
                } else if ("show".equals(tagName) && !"unavailable".equals(type)) {
                    show = xmlParser.nextText();
                } else if ("conflict".equals(tagName)) {
                    errorType = tagName;
                }
            }
            if (eventType == XmlPullParser.END_TAG && "presence".equals(tagName)) {
                break;
            }
        }
        if (isError) {
            onPresenceError(from, to, errorType);
        } else if ("unavailable".equals(type)) {
            onPresenceUnavailable(from, to);
        } else {
            if (onPresenceAvailable(from, to)) {
                onPresenceStatus(from, to, status, show);
            }
        }
    }

    protected String getEntityRef(XmlPullParser np) {
        StringBuilder ret = new StringBuilder();
        int[] startAndLength = new int[2];
        ret.append("&");
        char ch[] = np.getTextCharacters(startAndLength);
        ret.append(new String(ch, startAndLength[0], startAndLength[1]));
        ret.append(";");
        return ret.toString();
    }

    protected void onMessage() throws Exception {
        String id = xmlParser.getAttributeValue("", "id");
        String from = xmlParser.getAttributeValue("", "from");
        String to = xmlParser.getAttributeValue("", "to");
        String type = xmlParser.getAttributeValue("", "type");
        if (to == null) {
            to = serverIP;
        }
        StringBuilder msg = new StringBuilder();
        while (true) {
            int eventType = xmlParser.nextToken();
            String tagName = xmlParser.getName();

            if (eventType == XmlPullParser.END_TAG && "message".equals(tagName)) {
                break;
            }
            if (eventType == XmlPullParser.ENTITY_REF) {
                msg.append(getEntityRef(xmlParser));
            } else {
                if (!(eventType == XmlPullParser.START_TAG && xmlParser.isEmptyElementTag())) {
                    msg.append(xmlParser.getText());
                }
            }
        }
        //logger.info(msg.toString());
        //logger.info("type = " + type);
        if ("groupchat".equals(type)) {
            onMessageGroupchat(id, from, to, msg.toString());
        } else {
            onMessagePrivate(id, from, to, msg.toString());
        }
    }

    protected abstract void onIqBind(String id, String from, String to, String resource);

    protected abstract void onIqSession(String id, String from, String to);

    protected abstract void onIqQueryInfo(String id, String from, String to);

    protected abstract void onIqQueryItem(String id, String from, String to);

    protected abstract void onIqQueryError(String id, String from, String to);

    protected abstract void onIqPing(String id, String from, String to);

    protected abstract void onIqPingPort(String id, String from, String to, String port);

    protected abstract void onIqCardSet(String id, String from, String to);

    protected abstract void onIqCardGet(String id, String from, String to);

    protected abstract void onIqCardResult(String id, String from, String to);

    protected abstract void onPresenceStatus(String from, String to, String status, String show);

    protected abstract boolean onPresenceAvailable(String from, String to);

    protected abstract void onPresenceUnavailable(String from, String to);

    protected abstract void onPresenceError(String from, String to, String errorType);

    protected abstract void onMessageGroupchat(String id, String from, String to, String msg);

    protected abstract void onMessagePrivate(String id, String from, String to, String msg);

    protected abstract void onStream();
    protected static String XML_HEADER = "<?xml version='1.0'?>";
    protected static String STREAM_HEADER = "<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' id='s2c_%d' from='%s' version='1.0'>";
    protected static String STREAM_FEATURE = "<stream:features><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/></stream:features>";

    protected void sendHeader() {
        connection.send(XML_HEADER);
        connection.send(String.format(STREAM_HEADER, ++conId, serverIP));
        connection.send(STREAM_FEATURE);
    }
    protected static String IQ_SEND = "<iq type='%s' id='%s' from='%s' to='%s'>%s</iq>";

    protected void sendIq(String id, String type, String from, String to, String msg) {
        connection.send(String.format(IQ_SEND, type, id, from, to, msg));
    }
    protected static String PRESENCE_AVAILABLE = "<presence from='%s' to='%s'>";
    protected static String PRESENCE_UNAVAILABLE = "<presence from='%s' to='%s' type='%s'>";
    protected static String PRESENCE_END = "</presence>";

    protected void sendPresence(String from, String to, String type, String in) {
        StringBuilder sb = new StringBuilder();

        if (type != null) {
            sb.append(String.format(PRESENCE_UNAVAILABLE, from, to, type));
        } else {
            sb.append(String.format(PRESENCE_AVAILABLE, from, to));
        }
        if (in != null) {
            sb.append(in);
        }
        sb.append(PRESENCE_END);
        connection.send(sb.toString());
    }
    protected static String MESSAGE_CHAT = "<message from='%s' to='%s'>%s</message>";
    protected static String MESSAGE_GROUPCHAT = "<message from='%s' to='%s' type='%s'>%s</message>";

    protected void sendMessage(String from, String to, String type, String msg) {
        if (type == null) {
            connection.send(String.format(MESSAGE_CHAT, from, to, msg));
        } else {
            connection.send(String.format(MESSAGE_GROUPCHAT, from, to, type, msg));
        }
    }

    @Override
    public void run() {
        try {
            int eventType = xmlParser.getEventType();
            do {
                String tagName = xmlParser.getName();
                // Getting the start tag
                if (eventType == XmlPullParser.START_TAG) {
                    // stream is the first tag
                    if ("stream".equals(tagName)) {
                        onStream();
                    } else if ("iq".equals(tagName)) {
                        onIq();
                    } else if ("presence".equals(tagName)) {
                        onPresence();
                    } else if ("message".equals(tagName)) {
                        onMessage();
                    }
                } else if (eventType == XmlPullParser.END_TAG && "stream".equals(tagName)) {
                    // end stream.
                    break;
                }
                eventType = xmlParser.next();
            } while (!endFlag && eventType != XmlPullParser.END_DOCUMENT);
        } catch (Exception ex) {
            logger.warning("Xml parsing error");
            ex.printStackTrace();
        }
    }
    /** Regex pattern for parsing jid */
    private static final Pattern JID_PATTERN =
            Pattern.compile("(?:(.*)@)?(.*?)(?:/(.*))?");

    /** Parsing Jid */
    protected class Jid {

        /** room name and user name */
        private String room = null, name = null;

        /** Jid constructor
         *
         * @param room room name
         * @param name user name
         */
        public Jid(String room, String name) {
            this.room = room;
            this.name = name;
        }

        /** Jid constructor from string
         *
         * @param jid room@name
         */
        public Jid(String jid) {
            if (jid == null) {
                return;
            }
            Matcher matcher = JID_PATTERN.matcher(jid);
            matcher.matches();
            this.room = matcher.group(1);
            this.name = matcher.group(3);
        }

        /** Room jid */
        public boolean isRoom() {
            return room != null && name == null;
        }

        /** User jid */
        public boolean isUser() {
            return room != null && name != null;
        }

        /** Global jid */
        public boolean isGlobal() {
            return room == null && name == null;
        }

        public String getName() {
            return name;
        }

        public String getRoom() {
            return room;
        }

        @Override
        public String toString() {
            String str = serverIP;
            if (name != null) {
                str = str + "/" + name;
            }
            if (room != null) {
                str = room + "@" + str;
            }
            return str;
        }
    }
}
