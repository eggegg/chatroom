package chatroom.parser;

import chatroom.client.ChatroomClient;
import chatroom.client.EventInterface;
import chatroom.client.dataclass.User;
import chatroom.connection.Connection;
import chatroom.utils.Base64;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/** Client parser
 *
 * @author eggegg
 */
public class ClientParser extends Parser {

    private EventInterface event;

    public ClientParser(Connection con, String serverIP) {
        super(con);
        this.serverIP = serverIP;
        event = ChatroomClient.getClient();
    }

    @Override
    protected void onIqBind(String id, String from, String to, String resource) {
        // Nothing to do.
    }

    @Override
    protected void onIqSession(String id, String from, String to) {
        // Nothing to do.
    }

    @Override
    protected void onIqQueryInfo(String id, String from, String to) {
        // Nothing to do.
    }

    @Override
    protected void onIqQueryItem(String id, String from, String to) {
        Jid jid = new Jid(from);
        if (jid.isRoom()) {
            ArrayList<User> allUser = new ArrayList<User>();

            while (true) {
                try {
                    int eventType = xmlParser.next();
                    String tagName = xmlParser.getName();
                    String nsName = xmlParser.getNamespace();

                    if (eventType == XmlPullParser.START_TAG) {
                        if ("item".equals(tagName)) {
                            String user = xmlParser.getAttributeValue("", "jid");
                            Jid userJid = new Jid(user);
                            if (jid.getRoom().equals(userJid.getRoom())) {
                                allUser.add(new User(userJid.getName()));
                            } else {
                                logger.warning("Got some user from other room ...");
                            }
                        }
                    }
                    if (eventType == XmlPullParser.END_TAG && "query".equals(tagName) && IQ_QUERY_ITEM_NS.equals(nsName)) {
                        break;
                    }
                } catch (XmlPullParserException ex) {
                    Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            event.receiveRoomParticipants(jid.getRoom(), allUser);
        } else if (jid.isGlobal()) {
            ArrayList<String> allRoom = new ArrayList<String>();
            ArrayList<String> allDescription = new ArrayList<String>();
            ArrayList<Integer> allCount = new ArrayList<Integer>();

            while (true) {
                try {
                    int eventType = xmlParser.next();
                    String tagName = xmlParser.getName();
                    String nsName = xmlParser.getNamespace();

                    if (eventType == XmlPullParser.START_TAG) {
                        if ("item".equals(tagName)) {
                            String room = xmlParser.getAttributeValue("", "jid");
                            Jid roomJid = new Jid(room);
                            if (roomJid.isRoom()) {
                                allRoom.add(roomJid.getRoom());
                                allDescription.add(xmlParser.getAttributeValue("", "name"));
                                Integer i = Integer.valueOf(xmlParser.getAttributeValue("", "count"));
                                allCount.add(i);
                            } else {
                                logger.warning("Got non-room jid in room list...");
                            }
                        }
                    }
                    if (eventType == XmlPullParser.END_TAG && "query".equals(tagName) && IQ_QUERY_ITEM_NS.equals(nsName)) {
                        break;
                    }
                } catch (XmlPullParserException ex) {
                    Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            event.receiveRoomList(allRoom, allDescription, allCount);
        }
    }

    @Override
    protected void onIqQueryError(String id, String from, String to) {
        // Nothing to do.
    }

    @Override
    protected void onIqPing(String id, String from, String to) {
        // Nothing to do.
    }

    @Override
    protected void onStream() {
        // Nothing to do.
    }

    @Override
    protected void onPresenceStatus(String from, String to, String status, String show) {
        Jid jid = new Jid(from);
        if (jid.isUser()) {
            event.notifyStatusChange(jid.getRoom(), jid.getName(), status, show);
        } else {
            logger.warning("Got non-user presence status.");
        }
    }

    @Override
    protected boolean onPresenceAvailable(String from, String to) {
        Jid jid = new Jid(from);
        if (jid.isUser()) {
            event.userEnterRoom(jid.getRoom(), jid.getName());
        } else {
            logger.warning("Got non-user presence.");
        }
        return true;
    }

    @Override
    protected void onPresenceUnavailable(String from, String to) {
        Jid jid = new Jid(from);
        if (jid.isUser()) {
            event.userExitRoom(jid.getRoom(), jid.getName());
        } else {
            logger.warning("Got non-user presence unavailble.");
        }
    }
    private static String HTML_P_START = "<p style=\"margin-top: 0\">";
    private static String HTML_P_FIRST_START = "<p class=\"first\" style=\"margin-top: 0\">";

    private String[] getBody(String msg) {
        msg = msg.replaceAll("<img([^>]*?)/?>", "<img$1/>");
        XmlPullParser np = new MXParser();
        try {
            np.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
            np.setInput(new StringReader("<XD>" + msg + "</XD>"));
        } catch (XmlPullParserException ex) {
            Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        String ret = null;
        String pure = null;
        try {
            int eventType = np.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if ("body".equals(np.getName())) {
                        StringBuilder retb = new StringBuilder();
                        StringBuilder pureb = new StringBuilder();
                        boolean hasP = false;
                        while (true) {
                            eventType = np.nextToken();
                            if (eventType == XmlPullParser.END_TAG && "body".equals(np.getName())) {
                                break;
                            }
                            if (eventType == XmlPullParser.START_TAG && "p".equals(np.getName())) {
                                if (hasP == false) {
                                    hasP = true;
                                    retb.append(HTML_P_FIRST_START);
                                } else {
                                    retb.append(HTML_P_START);
                                }
                            } else if (eventType == XmlPullParser.ENTITY_REF) {
                                String str = getEntityRef(np);
                                retb.append(str);
                                pureb.append(str);
                            } else if (eventType == XmlPullParser.START_TAG || eventType == XmlPullParser.END_TAG) {
                                if (!(eventType == XmlPullParser.START_TAG && np.isEmptyElementTag())) {
                                    retb.append(np.getText().trim());
                                }
                            } else if (eventType != XmlPullParser.IGNORABLE_WHITESPACE) {
                                String str = np.getText().trim().replaceAll("\n(  )*", "\n");
                                // hack for indent.
                                retb.append(str.replaceAll(" ", "&nbsp;"));
                                pureb.append(str);
                            }
                        }
                        if (hasP) {
                            ret = retb.toString();
                        } else {
                            ret = HTML_P_FIRST_START + retb.toString() + "</p>";
                        }
                        pure = pureb.toString();
                    }
                }
                eventType = np.next();
            }
        } catch (XmlPullParserException ex) {
            Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new String[]{ret, pure};
    }

    @Override
    protected void onMessageGroupchat(String id, String from, String to, String msg) {
        Jid jid = new Jid(from);
        String[] str = getBody(msg);
        event.feedMessage(jid.getRoom(), jid.getName(), str[0]);
    }

    @Override
    protected void onPresenceError(String from, String to, String errorType) {
        if ("conflict".equals(errorType)) {
            event.nameConflicted(new Jid(from).getRoom(), new Jid(to).getName());
        }
    }

    @Override
    protected void onMessagePrivate(String id, String from, String to, String msg) {
        Jid jid = new Jid(from);
        Jid toJid = new Jid(to);
        event.feedPrivateMessage(jid.getRoom(), jid.getName(), toJid.getName(), getBody(msg)[0]);
    }
    private static String IQ_QUERY = "<iq type='get' id='%s' from='%s' to='%s'><query xmlns='%s'/></iq>";

    private void sendIqQuery(String id, String from, String to, String query) {
        connection.send(String.format(IQ_QUERY, id, from, to, query));
    }
    private static String DISCO_ITEM = "http://jabber.org/protocol/disco#items";

    public void sendRoomListQuery(String id) {
        sendIqQuery(id, "", serverIP, DISCO_ITEM);
    }

    public void sendRoomUserListQuery(String id, String roomid) {
        Jid jid = new Jid(roomid, null);
        sendIqQuery(id, "", jid.toString(), DISCO_ITEM);
    }
    private static String IQ_VCARD_GET = "<iq type='get' id='%s' to='%s'><vCard xmlns='vcard-temp'/></iq>";

    public void sendUserVCardGet(String id, String roomid, String userid) {
        Jid jid = new Jid(roomid, userid);
        connection.send(String.format(IQ_VCARD_GET, id, jid.toString()));
    }
    private static String IQ_VCARD_SET = "<iq type='set' id='%s'><vCard xmlns='vcard-temp'>%s</vCard></iq>";
    private static String IQ_VCARD_PHOTO_START = "<PHOTO>";
    private static String IQ_VCARD_PHOTO_END = "</PHOTO>";
    private static String IQ_VCARD_PHOTO_BASE = "<TYPE>%s</TYPE><BINVAL>%s</BINVAL>";
    private static String IQ_VCARD_PHOTO_EXTVAL = "<EXTVAL>%s</EXTVAL>";

    public void sendUserVCardSet(String id, String profilePic, String profilePicType, String profilePicURL) {
        StringBuilder vCard = new StringBuilder();
        if (profilePic != null) {
            vCard.append(IQ_VCARD_PHOTO_START);
            vCard.append(String.format(IQ_VCARD_PHOTO_BASE, profilePicType, profilePic));
            if (profilePicURL != null) {
                vCard.append(String.format(IQ_VCARD_PHOTO_EXTVAL, profilePicURL));
            }
            vCard.append(IQ_VCARD_PHOTO_END);
        }
        connection.send(String.format(IQ_VCARD_SET, id, vCard.toString()));
    }

    public void sendPresenceAvailble(String roomid, String name) {
        sendPresence(null, new Jid(roomid, name).toString(), null, null);
    }

    public void sendPresenceUnavailble(String roomid, String name) {
        sendPresence(null, new Jid(roomid, name).toString(), "unavailable", null);
    }
    private static String MESSAGE_FORMAT = "<body>%s</body><html xmlns='http://jabber.org/protocol/xhtml-im'><body xmlns='http://www.w3.org/1999/xhtml'>%s</body></html>";

    public void sendGroupMessage(String roomid, String name, String msg) {
        String[] realMsg = getBody(msg);
        sendMessage(new Jid(roomid, name).toString(), new Jid(roomid, null).toString(), "groupchat", String.format(MESSAGE_FORMAT, realMsg[1], realMsg[0]));
    }

    public void sendPrivateMessage(String roomid, String name, String to, String msg) {
        String[] realMsg = getBody(msg);
        sendMessage(new Jid(roomid, name).toString(), new Jid(roomid, to).toString(), null, String.format(MESSAGE_FORMAT, realMsg[1], realMsg[0]));
    }
    // Almost identical to serverparser. Too lazy to merge.
    protected static String PRESENCE_STATUS = "<status>%s</status>";
    protected static String PRESENCE_SHOW = "<show>%s</show>";

    public void updateStatus(String roomid, String name, String status, String show) {
        StringBuilder sb = new StringBuilder();
        if (status != null) {
            sb.append(String.format(PRESENCE_STATUS, status));
        }
        if (!"available".equals(show)) {
            sb.append(String.format(PRESENCE_SHOW, show));
        }
        sendPresence(new Jid(roomid, name).toString(), new Jid(roomid, name).toString(), null, sb.toString());
    }

    @Override
    protected void onIqCardSet(String id, String from, String to) {
        // Nothing to do.
        // Should not happened.
    }

    @Override
    protected void onIqCardGet(String id, String from, String to) {
        // Nothing to do.
        // Should not happened.
    }

    @Override
    protected void onIqCardResult(String id, String from, String to) {
        try {
            if (xmlParser.isEmptyElementTag()) {
                // Card set successfully
                // Nothing to do.
                return;
            }
            Jid jid = new Jid(from);
            if (jid.isUser()) {
                boolean inPhoto = false;
                String base64Img = null;
                String imgURL = null;
                while (true) {
                    int eventType = xmlParser.next();
                    String tagName = xmlParser.getName();

                    if (eventType == XmlPullParser.START_TAG) {
                        if ("PHOTO".equals(tagName)) {
                            inPhoto = true;
                        } else if ("BINVAL".equals(tagName)) {
                            if (inPhoto) {
                                base64Img = xmlParser.nextText();
                            }
                        } else if ("EXTVAL".equals(tagName)) {
                            if (inPhoto) {
                                imgURL = xmlParser.nextText();
                            }
                        }
                    }
                    if (eventType == XmlPullParser.END_TAG) {
                        if ("PHOTO".equals(tagName)) {
                            inPhoto = false;
                        } else if ("vCard".equals(tagName)) {
                            break;
                        }
                    }
                }
                BufferedImage pic = null;
                if (base64Img != null) {
                    pic = ImageIO.read(new Base64.InputStream(new ByteArrayInputStream(base64Img.getBytes())));
                }
                event.setProfilePic(jid.getRoom(), jid.getName(), pic, imgURL);
            } else {
                logger.warning("Got non-user vcard...");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XmlPullParserException ex) {
            Logger.getLogger(ClientParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void onIqPingPort(String id, String from, String to, String port) {
        event.openAudio(Integer.valueOf(port));
    }
    protected static String IQ_PING_PORT = "<ping xmlns='urn:xmpp:ping' port='ask'/>";

    public void getAudioPort(String id, String roomid, String name) {
        sendIq(id, "get", new Jid(roomid, name).toString(), null, String.format(IQ_PING_PORT));
    }
}
