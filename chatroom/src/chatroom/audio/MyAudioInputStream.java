/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatroom.audio;

import chatroom.connection.Connection;
import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

/**
 *
 * @author peter
 */
public class MyAudioInputStream extends AudioInputStream {

    private Connection cnt;
    private byte[] buffer = new byte[AudioConfig.bufferSize];
    private int tempByte;

    public MyAudioInputStream(InputStream stream, AudioFormat format, long length, Connection cnt) {
        super(stream, format, length);
        this.cnt = cnt;
    }

    public void setBuffer(int b, int x) {
        buffer[x] = (byte) ((b - (tempByte - 128)) & 0xff);
    }

    public void sendBuffer() {
        cnt.send(buffer);
    }

    @Override
    public int read() throws IOException {
        return tempByte = super.read();
    }

    public void end() {
        cnt.end();
    }
}
