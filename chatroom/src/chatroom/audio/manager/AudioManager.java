package chatroom.audio.manager;

import chatroom.audio.AudioClient;
import chatroom.audio.AudioConfig;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;

/** Audio manager for a audio conference
 *
 * @author eggegg
 */
public class AudioManager extends JFrame {

    private static final Logger logger = Logger.getLogger("AudioManager");
    private static AudioManager audioManager = null;

    public static AudioManager getManager(String server, int port) throws IOException, LineUnavailableException {
        if (audioManager != null) {
            return null;
        }
        audioManager = new AudioManager(server, port);
        return audioManager;
    }
    private JScrollBar playScrollbar;
    private JScrollBar recordScrollbar;
    private AudioClient client;

    public AudioManager(String server, int port) throws IOException, LineUnavailableException {
        setTitle("Audio Conference");
        setSize(400, 100);

        client = new AudioClient(server, port);
        client.startAudio();
        playScrollbar = new JScrollBar(JScrollBar.HORIZONTAL, 30, 20, 0, 120);
        playScrollbar.addAdjustmentListener(new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged(AdjustmentEvent ae) {
                client.setPlayVolume(ae.getValue());
                logger.log(Level.INFO, "Setting playing volumn: {0}", ae.getValue());
            }
        });
        recordScrollbar = new JScrollBar(JScrollBar.HORIZONTAL, 30, 20, 0, 120);
        recordScrollbar.addAdjustmentListener(new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged(AdjustmentEvent ae) {
                client.setRecordVolume(ae.getValue());
                logger.log(Level.INFO, "Setting recording volumn: {0}", ae.getValue());
            }
        });
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        getContentPane().add(new JLabel("Audio Volume"));
        getContentPane().add(playScrollbar);
        getContentPane().add(new JLabel("Microphone Volume"));
        getContentPane().add(recordScrollbar);

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                client.stopAudio();
                audioManager = null;
            }
        });
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    AudioManager manager = new AudioManager("140.112.18.203", AudioConfig.port);
                    manager.setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(AudioManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
