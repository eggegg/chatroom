package chatroom.audio;

import javax.sound.sampled.AudioFormat;

/** Audio recorder and listener configurations
 *
 * @author eggegg
 */
public abstract class AudioConfig {
    // encode PCM unsigned, 8000Hz, 1ch
    public static final AudioFormat audioFormat
            = new AudioFormat(AudioFormat.Encoding.PCM_UNSIGNED,
                              8000.0F, 8, 1, 1, 8000.0F, false);

    // audio buffer size
    public static final int bufferSize = 256;

    // port number for testing purpose
    public static final int port = 4127;
}
