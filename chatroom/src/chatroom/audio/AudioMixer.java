package chatroom.audio;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sound.sampled.AudioFormat;

public class AudioMixer {

    private final List<MyAudioInputStream> m_audioInputStreamList;
    private ArrayList<Byte> allByte;

    public AudioMixer(AudioFormat audioFormat, List<MyAudioInputStream> audioInputStreams) {
        m_audioInputStreamList = audioInputStreams;
    }

    private void read(int x) throws IOException {
        int nSample = 0;
        Iterator streamIterator = m_audioInputStreamList.iterator();
        while (streamIterator.hasNext()) {
            MyAudioInputStream stream = (MyAudioInputStream) streamIterator.next();
            int nByte = stream.read();
            if (nByte == -1) {
                streamIterator.remove();
                continue;
            } else {
                nSample += nByte - 128;
            }
        }
        for (MyAudioInputStream stream : m_audioInputStreamList) {
            stream.setBuffer(nSample + 128, x);
        }
    }

    public void broadcast() throws IOException {
        /*for (int i = 0; i < AudioConfig.bufferSize; i++) {
        int r = read();
        buf[i] = (byte) ((r == -1) ? 128 : r);
        }*/
        synchronized (m_audioInputStreamList) {
            for (int i = 0; i < AudioConfig.bufferSize; i++) {
                read(i);
            }
            for (MyAudioInputStream stream : m_audioInputStreamList) {
                stream.sendBuffer();
            }
        }
    }

    public void end() {
        for (MyAudioInputStream stream : m_audioInputStreamList) {
            stream.end();
        }
    }
}
