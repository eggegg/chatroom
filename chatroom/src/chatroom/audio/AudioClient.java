package chatroom.audio;

import chatroom.connection.Connection;
import java.io.IOException;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

/** Audio recorder and sender
 *
 * @author eggegg
 */
public class AudioClient extends Thread {

    /** Logger */
    private static final Logger logger = Logger.getLogger("Recorder");

    /** Volumn resizer */
    private static void resize(byte[] buf, int gain) {
        for (int i = 0; i < buf.length; ++i) {
            buf[i] = (byte) (((((int) (buf[i] & 0xff)) - 128) * gain / 100) + 128);
        }
    }
    /** TCP connection object */
    private Connection connection;
    /** Record data line */
    private TargetDataLine recordDataLine;
    /** Play data line */
    private SourceDataLine playDataLine;
    /** Volumn controls */
    private int playVolume = 100;
    private int recordVolume = 100;
    /** Data line input info */
    private Info inputInfo;
    /** Data line output info */
    private Info outputInfo;
    /** Recording flag */
    private boolean flag;

    /** Initialize a UDP connection to server
     *
     * @param server server ip string
     */
    public AudioClient(String server, int port) throws IOException {
        connection = new PlayConnection(server, port);
        inputInfo = new Info(TargetDataLine.class, AudioConfig.audioFormat);
        outputInfo = new Info(SourceDataLine.class, AudioConfig.audioFormat);
    }

    /** Start audio conference
     *
     * @throws LineUnavailableException if the audio system is used by others
     */
    public void startAudio() throws LineUnavailableException {
        recordDataLine = (TargetDataLine) AudioSystem.getLine(inputInfo);
        recordDataLine.open(AudioConfig.audioFormat);
        recordDataLine.start();
        playDataLine = (SourceDataLine) AudioSystem.getLine(outputInfo);
        playDataLine.open(AudioConfig.audioFormat);
        playDataLine.start();

        flag = true;

        start();
    }

    /** Set the microphone volumn
     *
     * @param vol value from 0 - 100
     */
    public void setRecordVolume(int vol) {
        recordVolume = vol;
    }

    /** Set the playing volumn
     *
     * @param vol value from 0 - 100
     */
    public void setPlayVolume(int vol) {
        playVolume = vol;
    }

    /** Stop recording and playing */
    public void stopAudio() {
        recordDataLine.close();
        playDataLine.close();
        flag = false;
    }

    /** Running the UDP sending process */
    @Override
    public void run() {
        connection.start();
        byte[] buffer = new byte[AudioConfig.bufferSize];

        do {
            int byteRead = recordDataLine.read(buffer, 0, buffer.length);

            //System.err.println((int) (buffer[0] & 0xff));
            if (byteRead > 0) {
                resize(buffer, recordVolume);
                connection.send(buffer, 0, byteRead);
            }
        } while (flag);

        connection.end();
    }

    private class PlayConnection extends Connection {

        private int tot = 0;

        public PlayConnection(String server, int port) throws IOException {
            super(server, port);
        }

        @Override
        protected void onRead(byte[] data, int size) {
            //System.err.printf("%d, %d\n", data[0], size);
            tot++;
            if (tot % 20 == 0) {
                return;
            }
            resize(data, playVolume);
            playDataLine.write(data, 0, size); // BUG!!!! was (data, size, 0) before...
        }
    }

    public static void main(String args[]) {
        try {
            AudioClient client = new AudioClient("140.112.18.203", AudioConfig.port);
            client.startAudio();
        } catch (Exception ex) {
            logger.severe("cannot create audio client");
        }

    }
}
